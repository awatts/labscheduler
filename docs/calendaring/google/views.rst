.. _calendaring_google-views:

=====================
Google Calendar Views
=====================

.. module:: labscheduler.calendaring.google.views
   :synopsis: Views related to connecting to google calendar


Views
=====

index
-----
.. function:: index(request)

   Shows a page with a link to a Google page where an `AuthSub <http://code.google.com/apis/accounts/docs/AuthSub.html>`_ token can be generated for the site.

   .. todo::

      This should check if the token is valid, and redirect to :func:`view_calendars` instead if it is.

authsub
-------
.. function:: authsub(request)

   A view with no user visible content except on failure, where you get a :class:`~django.http.HttpResponseBadRequest`. Normally it updates the :class:`~labscheduler.calendaring.google.models.GoogleCredentials` associated with the :class:`~django.contrib.auth.models.User` in request and then redirects to :func:`view_calendars`.

view_calendars
--------------
.. function:: view_calendars(request)

   Outputs a list of all calendars the user has write access to. Just a sanity check that there are writable calendars.

next_two_weeks
--------------
.. function:: next_two_weeks(request)

   Outputs a list of events for the next two weeks that make the user Busy. Again, just a sanity check.
