#
# basic_calendar/models.py
#
# Author: Andrew Watts
#
# Copyright (c) 2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.db import models
from django.contrib.auth.models import User
from labscheduler.resources.models.other import Room

__all__ = ['BasicCalendar','Event',]

class BasicCalendar(models.Model):
    """
    A simple, no-frills calendar to use when you don't want to run your own
    calendar server or connect to an outside calendar server.
    """
    
    owner = models.ForeignKey(User, related_name="%(class)s_related", unique=True)
    earliest = models.TimeField(help_text="Earliest time to allow events")
    latest = models.TimeField(help_text="Latest time to allow events")
    weekends = models.BooleanField(help_text="Allow weekend events", default=False)
    def __unicode__(self):
        return self.owner.__unicode__()

    class Meta:
        app_label = 'calendaring'

class Event(models.Model):
    """
    A timeslot when an event occurs.
    """

    calendar = models.ForeignKey(BasicCalendar, related_name="%(class)s_related")
    room = models.ForeignKey(Room, related_name="%(class)s_related")
    invitees = models.ManyToManyField(User, related_name="%(class)s_invited_related", null=True, blank=True)
    event_date = models.DateField()
    start_time = models.TimeField() 
    end_time = models.TimeField()

    busy = models.BooleanField(default = True, help_text = u'Mark as busy? i.e. Disallow other overlapping events.')
    purpose = models.TextField(blank=True, default=u'')

    def __unicode__(self):
        return "%s@%s" % (self.event_date, self.start_time)

    class Meta:
        ordering = ['room', 'event_date', 'start_time']
        app_label = 'calendaring'

