from django.forms.util import ErrorDict, ErrorList
from django.utils.encoding import force_unicode

def ErrorsToDict(obj):
    if isinstance(obj, ErrorDict):
        return dict((key,[force_unicode(e) for e in obj[key]]) for key in obj)

def get_bzr_revision():
    """
    Get revision info from bazaar branch.
    """
    from bzrlib import branch
    local_branch, relpath = branch.Branch.open_containing('.')
    rev_no, rev_id = local_branch.last_revision_info()
    tag_dict = local_branch.tags.get_reverse_tag_dict()
    if rev_id in tag_dict:
        return tag_dict[rev_id][0]
    return '+bzr-' + str(rev_no)
