Aborted subject scheduling system developed before the relevant labs went with the more likely to be IRB approved [SONA](https://www.sona-systems.com/default.aspx)

Dependencies:

 * Python >= 2.6 - In process of converting to be 2 or 3 compatible
 * [Django >= 1.4](http://www.djangoproject.com/)
 * [gdata-python-client](http://code.google.com/p/gdata-python-client/)
 * [Python dateutil](http://labix.org/python-dateutil)
 * [Sphinx](http://sphinx.pocoo.org/) if you want to build the docs

Link a 200x175 png file to media/img/logo.png to make the header work correctly. A translucent black 200x175 png called placeholder.png is provided for development.