#
# version.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2009, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from labscheduler.utils import get_bzr_revision

VERSION = (1, 0, 0, 'alpha', 2)

def version():
    """
    Get version info. Based partly on Django's get_version function
    """

    version = '%s.%s.%s' % (VERSION[0], VERSION[1],VERSION[2])
    if VERSION[3]:
        version = '%s%s%s' % (version, VERSION[3], VERSION[4])
    version = '%s (%s)' % (version, get_bzr_revision())
    return version
