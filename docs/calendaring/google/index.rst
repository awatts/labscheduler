.. _calendaring_google-index:

========================================================================
:mod:`~labscheduler.calendaring.google` -- Connecting to Google Calendar
========================================================================

.. module:: labscheduler.calendaring.google
   :synopsis: A calendaring backend backend for Google Calendar
.. moduleauthor:: Andrew Watts <awatts@bcs.rochester.edu>

.. toctree::
   :maxdepth: 1
   
   models
   views

What the module provides
========================
