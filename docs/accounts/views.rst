.. _accounts-views:

=================================
LabScheduler user accounts views
=================================

.. module:: labscheduler.accounts.views
    :synopsis: Views related to accounts

Overview
========

The accounts system has views for account management functions

Views
=====
In all cases below, a parameter named request is of type :class:`django.http.HttpRequest`


.. function:: index(request)

    If a user is logged in, redirects them to :func:`profile`, else redirects them to :func:`new_user`

.. function:: login(request)

    A custom login using email address and password, in contrast the Django builtin :func:`django.contrib.auth.login`, which uses username and password. The idea behind this being that people will forget one more username, but should remember their own email address, particularly if encouraged to use their instituitonal email to register.

.. function:: check_email_address(request)

    Designed to only be called by AJAX. Looks for a parameter called 'email' in the GET, and returns and empty string if the address is not already in the database, or a warning that the address is in use if it is already in the database.

.. function:: new_user(request)

    Logic for creating a new user. They provide their first and last names, and a password. A username is generated from their first initial and last name, with the final letter being replaced by a number in the instance that the username already exists.

.. function:: pwchange(request)

.. function:: initial_survey(request)

    Logic for filling out a subject survey. Creates :class:`~labscheduler.accounts.models.Survey`, :class:`~labscheduler.accounts.models.Phone`, and :class:`~labscheduler.accounts.models.Email` objects, in order to have basic information about the subject plus contact information.

.. function:: add_object(request, objtype)
    
    :param objtype: Must be "phone", "email", or "lang".

    Abstracted logic for adding an object associated with the logged in user. On success it returns the saved object JSON encoded. If the user has no `~labscheduler.accounts.models.Survey` it redirects them to create one. If an invalid object type is passed in it returns a :class:`~django.http.HttpResponseBadRequest`.


.. function:: del_object(request, objtype, id)
    
    :param objtype: Must be "phone", "email", or "lang".
    :param id: Must be the id number for an object of objtype associated with the user's Survey 

    Abstracted logic for deleting an object associated with the logged in user.  If the user has no :class:`~labscheduler.accounts.models.Survey` it redirects them to create one. If an invalid object type is passed in it returns a :class:`~django.http.HttpResponseBadRequest`.

.. function:: manage_personal(request)

    A view for updating personal information from the user's :class:`~labscheduler.accounts.models.Survey`

.. function:: manage_student(request)

    An AJAX-only view for updating student status information from the user's :class:`~labscheduler.accounts.models.Survey`. Called from :func:`profile`.

.. function:: manage_hearing_vision(request)
    
    An AJAX-only view for updating hearing and vision status information from the user's :class:`~labscheduler.accounts.models.Survey`. Called from :func:`profile`.

.. function:: manage_studies(request)
    
    An AJAX-only view for updating willingness to participate in experiments from the user's :class:`~labscheduler.accounts.models.Survey`. Called from :func:`profile`.

.. function:: profile(request)

    Display the Survey information for a user in order for them to verify that it is correct and allow them to manage objects that can occur multiple times, e.g. phone numbers or email addresses.  
