from django.conf.urls.defaults import *

urlpatterns = patterns('reports.views',
        #(r'^$', 'index'),
        url(r'languagesummary/$', 'language_summary'),
        url(r'newenrollment/$', 'new_enrollment'),
        url(r'willing/$', 'willing_subjects'),
        # grr. broken. I *hate* the django url system with a seething passion
        #(r'willing_text/$', 'willing_subjects', {'type' : 'text'}, "willing_subj_text"),
        url(r'dns/$', 'dns_list'),
    )
