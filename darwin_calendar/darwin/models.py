#
# calendar/darwin/models.py
#
# Author: Andrew Watts
# Created: Tue 03-Jun-2008 16:39 EDT
#
# Copyright (c) 2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.db import models
from django.contrib.auth.models import User, Group
from labscheduler.scheduler.models import Room, Equipment

from uuid import uuid4

__all__ = [
        'DjangoUserPrincipal',
        'DjangoGroupPrincipal',
        'DjangoLocationPrincipal',
        'DjangoResourcePrincipal',
    ]

"""
Classes for the principals to make the labscheduler database look like a
directory service to Darwin CalendarServer
"""

class DjangoAccountPrincipal(models.Model):
    """
    Abstract Base Class for other principals to inherit from.
    """

    guid = models.CharField(max_length=36, default=str(uuid4()), editable=False)

    def getUID(self):
        """Principal's User ID"""
        raise NotImplementedError
    
    def getName(self):
        """Principal's name"""
        raise NotImplementedError
    
    def getCUA(self):
        """Calendar User Address"""
        raise NotImplementedError
    
    def getCalEnabled(self):
        """Boolean function - is calendaring enabled for this account?"""
        raise NotImplementedError
    
    def getPassword(self):
        raise NotImplementedError
    
    def getMembers(self):
        """Only relevant for a Group"""
        raise NotImplementedError
    
    def getGroups(self):
        """Only relevant for a User"""
        raise NotImplementedError
    
    def getProxies(self):
        """Who can manipulate this calendar in the stead of the principal?"""
        raise NotImplementedError
    
    def getROP(self):
        """Who can read this calendar other than the principal?"""
        raise NotImplementedError

    def getROPFor(self):
        """Whose calendars can the principal read?"""
        raise NotImplementedError

    uid = property(getUID, None, None, None)
    name = property(getName, None, None, None)
    calendarUserAddresses = property(getCUA, None, None, None)
    autoSchedule = property(getAutoSched, None, None, None)
    enabledForCalendaring = property(getCalEnabled, None, None, None)
    password = property(getPassword, None, None, None)
    members = property(getMembers, None, None, None)
    groups = property(getGroups, None, None, None)
    proxies = property(getProxies, None, None, None)
    readOnlyProxies = property(getROP, None, None, None)
    readOnlyProxyFor = property(getROPFor, None, None, None)

    class Meta:
        abstract = True

class DjangoUserPrincipal(DjangoAccountPrincipal):
    """
    Principal for DirectoryService.recordType_user
    """
    django_user = models.ForeignKey(User, related_name='calprincs', unique=True)

    def getUID(self):
        return self.django_user.username

    def getName(self):
        return self.django_user.get_full_name()

    def getCUA(self):
        return "mailto:%s" % self.django_user.email

    def getCalEnabled(self):
        return True
    
    def getPassword(self):
        return self.django_user.password

    def getMembers(self):
        # a user doesn't have members
        return None

    def getGroups(self):
        #FIXME: implement this
        pass
    
    def getProxies(self):
        #FIXME: implement this
        pass
    
    def getROP(self):
        #FIXME: implement this
        pass
    
    def getROPFor(self):
        #FIXME: implement this
        pass

class DjangoGroupPrincipal(models.Model, DjangoAccountPrincipal):
    """
    Principal for DirectoryService.recordType_group
    """
    django_group = models.ForeignKey(Group, related_name='calprincs', unique=True)

    def getUID(self):
        return self.django_group.name

    def getName(self):
        return self.django_group.name

    def getCUA(self):
        #FIXME: implement this
        pass

    def getCalEnabled(self):
        return False

    def getPassword(self):
        #FIXME: implement this
        pass

    def getMembers(self):
        #FIXME: implement this
        pass

    def getGroups(self):
        # a group doesn't have groups
        return None

    def getProxies(self):
        #FIXME: implement this
        pass
    
    def getROP(self):
        #FIXME: implement this
        pass
    
    def getROPFor(self):
        #FIXME: implement this
        pass

class DjangoLocationPrincipal(models.Model, DjangoAccountPrincipal):
    pass

class DjangoResourcePrincipal(models.Model, DjangoAccountPrincipal):
    pass
