#
# calendaring/google/utils.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

import gdata.calendar.service
import gdata.service

from .models import cal_service_for_user

__all__ = ['cal_service_for_user',]

#username = 'username@gmail.com'
#visibility = 'private'
#projection = 'free-busy'
#query = gdata.calendar.service.CalendarEventQuery(username, visibility, projection)

#
# for calendars other than the default, you need the feed id, which is put in
# as the username
#
