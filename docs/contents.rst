.. _contents:

Labscheduler documentation contents
===================================

.. toctree::
   :maxdepth: 2

   index
   settings
   accounts/index
   calendaring/index
   reports/index
   resources/index

Indices, glossary and tables
============================

* :ref:`genindex`
* :ref:`modindex`
