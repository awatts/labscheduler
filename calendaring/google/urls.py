from django.conf.urls.defaults import *

urlpatterns = patterns('calendaring.google.views',
        url(r'^$', 'index'),
        url(r'authsub/$', 'authsub'),
        url(r'allcals/$', 'view_calendars'),
        url(r'twoweeks/$', 'next_two_weeks'),
        url(r'busytimes/$','busy_times'),
        url(r'upcoming/$', 'upcoming'),
)
