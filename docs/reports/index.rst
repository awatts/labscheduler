.. _reports-index:

===========================================================
:mod:`reports` -- Reports on enrollment, etc.
===========================================================

.. module:: reports
   :synopsis: Classes for generating reports about experiments
.. moduleauthor:: Andrew Watts <awatts@bcs.rochester.edu>

.. toctree::
   :maxdepth: 1

   models
   views
