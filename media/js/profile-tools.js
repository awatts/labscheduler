/*!
 *  Lab Scheduler Profile utilities
 *  Author: Andrew Watts
 *  Copyright 2008-2009 - University of Rochester : Brain and Cognitive Sciences
 *  Licensed under the LGPLv2.1: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *  Assumes jquery.js and jqeuery.form.js are already loaded
*/

$(document).ready(
    function() {
        $('#tabs').tabs();
        $('#insert_phone').hide();
        $('#insert_email').hide();
        $('#insert_lang').hide();
        $('.del').live('click',
            function(event){
                event.preventDefault();
                deleteObject(this.pathname);
                return false; //I think this is necessary
            }
        );
    }
);

function loadPhoneForm() {
    $('#insert_phone').load('/accounts/add/phone/',
        function(){
            $('#insert_phone').slideDown("slow");
            $('#add_phone').submit(
                function () {
                    var options = {
                        url: '/accounts/add/phone/',
                        type: 'POST',
                        dataType: 'json',
                        success: appendPhone
                    };
                    $(this).ajaxSubmit(options);
                    return false;
                }
            );
        }
    );
    $('#prephone').hide();
}

function appendPhone(data) {
    if (data.errors) {
        showErrors('#add_phone',data.errors);
    } else {
        $('#prephone').append('<li>' + data.phone + ' (' + data.type +') <a class="del" href="/accounts/del/phone/'+ data.id +'/"> <img src="/media/img/admin/icon_deletelink.gif"/> Delete</a></li>');
        closeForm('phone');
    }
}

function loadEmailForm() {
    $('#insert_email').load('/accounts/add/email/',
        function(){
            $('#insert_email').slideDown("slow");
            $('#add_email').submit(
                function () {
                   var options = {
                        url: '/accounts/add/email/',
                        type: 'POST',
                        dataType: 'json',
                        //beforeSubmit: showRequest,
                        success: appendEmail
                   };
                   $(this).ajaxSubmit(options);
                   return false;
                }
            );
        }
    );
    $('#preemail').hide();
}

function showRequest(formData, jqForm, options) {
    var queryString = $.param(formData);
    alert('About to submit: \n\n' + queryString);
    return true;
}

function appendEmail(data) {
    if (data.errors) {
        showErrors('#add_email',data.errors);
    } else {
        $('#preemail').append('<li>' + data.uname + ' &lt;'+ data.email + '&gt; <a class="del" href="/accounts/del/email/' + data.id +'/"> <img src="/media/img/admin/icon_deletelink.gif"/> Delete</a></li>');
        closeForm('email');
    }
}

function loadLangForm() {
    $('#insert_lang').load('/accounts/add/lang/',
        function(){
            $('#insert_lang').slideDown("slow");
            $('#add_lang').submit(
                function() {
                    var options = {
                        url: '/accounts/add/lang/',
                        type: 'POST',
                        dataType: 'json',
                        success: appendLang
                    };
                    $(this).ajaxSubmit(options);
                    return false;
                }
            );
        }
    );

    $('#prelang').hide();
}

function appendLang(data) {
    if(data.errors) {
        showErrors('#add_lang',data.errors);
    } else {
        $('#prelang').append("<li>" + data.lang + " - Started: " + data.started + ", Stopped: " + data.stopped + ", Proficiency: " + data.proficient + '<a class="del" href="/accounts/del/lang/' + data.id +'/"> <img src="/media/img/admin/icon_deletelink.gif"/> Delete</a></li>');
        closeForm('lang');
    }
}

function closeForm(formname) {
    $('#pre'+formname).show("fast",
        function(){
            $('#insert_'+formname).hide();
        }
    );
    $('#insert_'+formname).empty();

    return false;
}

function showErrors(divname,errors) {
    for (e in errors) {
        var errorlist = $(divname).find("input[name="+e+"]").before("<ul class=errorlist></ul>").siblings('ul.errorlist');
        $.each(errors[e], function(i, error) {
                errorlist.append('<li>' + error + '</li>');
        });
    }
}

function deleteObject(url) {
    if (confirm("Are you sure you want to delete this?")) {
        var data = {};

        $.post(url, data,
            function(respData) {
                $('#pre'+respData.type).find('a[href='+url+']').parent().remove();
            }, "json"
        );
    }
}

function managePersonal() {
    var data = {};
    $.get('/accounts/survey/personal/', data,
        function(respData) {
            $('#personal > ul').empty().hide().append(respData).slideDown("fast");
            $('#id_dob').datepicker({yearRange: '-80:0', dateFormat: 'yy-mm-dd', initStatus: 'Choose your Date of Birth', showStatus: true});
            $('#id_dob').blur(
                function() {
                    entered_date = $(this).attr('value');
                    if (/\d{4}-\d{2}-\d{2}/.exec(entered_date) == null) {
                      alert("Date must be in YYYY-MM-DD format");
                      return false;
                    }
                }
            );
            $('#update_personal').submit(
                function() {
                    var options = {
                        target: '#update_personal',
                        url: '/accounts/survey/personal/',
                        type: 'POST',
                        success: function() {
                            $('#update_personal').slideDown('slow');
                        }
                    };
                    $(this).ajaxSubmit(options);
                    return false;
                }
            );
        }, "html"
    );
}

function manageStudent() {
    var data = {};
    $.get('/accounts/survey/student/', data,
        function(respData) {
            $('#student > ul').empty().hide().append(respData).slideDown("fast");
            if ($('#id_student_0').attr('checked') == false) {
                $('#id_gradyear').hide();
                $('#id_gradterm').hide();
            }
            $('#id_student_0').click(
                function() {
                    $('#id_gradyear').show("fast");
                    $('#id_gradterm').show("fast");
                }
            );
            $('#id_student_1').click(
                function() {
                    $('#id_gradyear').hide("fast").val('');
                    $('#id_gradterm').hide("fast").val('');
                }
            );
            $('#update_student').submit(
                function() {
                    var options = {
                        target: '#update_student',
                        url: '/accounts/survey/student/',
                        type: 'POST'
                    };
                    $(this).ajaxSubmit(options);
                    return false;
                }
            );
        }, "html"
    );
}

function manageVisionHearing() {
    var data = {};
    $('#hearsee > ul').empty().hide();
    $.get('/accounts/survey/hearingvision/', data,
        function(data) {
            $('#hearsee > ul').append(data);
            $('#hearsee > ul').slideDown("fast");
            if ($('#id_hearing_normal_0').attr('checked')) {
                $('#id_hearing_problems').hide();
            }
            if ($("#id_vision_normal > option[selected]").attr("value") != 4) {
                $('#id_vision_other').hide();
            }
            $('#id_hearing_normal_0').click(
                function() {
                    $('#id_hearing_problems').slideUp("fast").val('');
                }
            );
            $('#id_hearing_normal_1').click(
                function(){$('#id_hearing_problems').slideDown();}
            );
            $('#id_vision_normal').change(
                function() {
                    if ($("#id_vision_normal > option[selected]").attr("value") == 4) {
                    $('#id_vision_other').slideDown();
                    } else {
                        $('#id_vision_other').slideUp("fast").val('');
                    }
                }
            );

            $('#update_visionhearing').submit(
                function() {
                    var options = {
                        target: '#update_visionhearing',
                        url: '/accounts/survey/hearingvision/',
                        type: 'POST'
                    };
                    $(this).ajaxSubmit(options);
                    return false;
                }
            );
        }
    );
}

function manageStudies() {
    $('#studies > ul').empty().slideUp("fast");
    var data = {};
    $.get('/accounts/survey/studies/', data,
        function(data) {
            $('#studies > ul').append(data);
            $('#studies > ul').slideDown("fast");
            $('#update_studies').submit(
               function() {
                    var options = {
                        target: '#studies > ul',
                        url: '/accounts/survey/studies/',
                        type: 'POST'
                    };
                    $(this).ajaxSubmit(options);
                    return false;
                }
            );
        }, "html"
    );
}
