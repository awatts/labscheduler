#
# resources/views/schedule.py
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.transaction import commit_on_success
from django.contrib.auth.models import User
from django.http import Http404

from resources.models import Experiment, ExperimentInstance, ExperimentCodes
from resources.forms import ScheduleUserForm, ScheduleExptForm, ExperimentForm
from resources.utils import get_available
from accounts.models import Survey
from calendaring.models import Event, RoomCal
#from settings import HOST_ROOT
#from django.conf import settings

from datetime import datetime, date, time, timedelta
from dateutil.parser import parse
from dateutil.tz import tzlocal
import re
import random

__all__ = ('schedule_an_experiment', 'cancel_an_experiment', 'staff_schedule_subject', 'mark_completed', 'create_new_experiment')


@login_required
@commit_on_success
def schedule_an_experiment(request):
    """
    Set the date and time for an experiment instance
    """

    if request.method == 'POST':
        form = ScheduleExptForm(request.POST)
        if form.is_valid():
            expt = form.cleaned_data['experiment']
            experiment = Experiment.objects.get(id=expt)
            room = form.cleaned_data['room']
            edate = form.cleaned_data['date']
            etime = form.cleaned_data['time']

            roomcal = RoomCal.objects.get(room=room)

            start_time = datetime.combine(edate, etime)
            start_time = start_time.replace(tzinfo=tzlocal())
            end_time = start_time + timedelta(minutes=experiment.length)

            available = get_available(request.user)
            if experiment not in available:
                return HttpResponseBadRequest("You can't add %s. You have either already scheduled or completed it or are ineligible." % experiment.pub_name)

            if not roomcal.has_free_time(start_time, end_time):
                return HttpResponseBadRequest("%s is not free on %s at %s" % (room.get_long_name(), edate.isoformat(), etime.isoformat()))

            ex = ExperimentInstance(
                experiment = experiment,
                subject = request.user,
                assigned_to = experiment.lab.lab_manager,
            )
            ex.save()

            event = Event.objects.create(
                    subject = request.user,
                    title = experiment.pub_name,
                    exptinst = ex,
                    room = room,
                    start_time = start_time,
                    end_time = end_time
                    )

        # check to see if experiment is less than 1hr and see if we can find
        # a suggested pairing with the experiment and another available that
        # can be scheduled.
        expt2 = None
        if experiment.length < 60:
            suggested = list(experiment.suggested_pairings.all())
            for s in suggested:
                if s not in available:
                    suggested.remove(s)

            if len(suggested):
                ex2 = random.choice(suggested)

                start_time = end_time
                end_time = start_time + timedelta(minutes=ex2.length)

                expt2 = ExperimentInstance(
                    experiment = ex2,
                    subject=request.user,
                    assigned_to = ex2.lab.lab_manager,
                )
                expt2.save()

                event = Event.objects.create(
                    subject = request.user,
                    title = ex2.pub_name,
                    exptinst = expt2,
                    room = room,
                    start_time = start_time,
                    end_time = end_time
                )
    else:
        form = ScheduleExptForm()
        return render_to_response('resources/available.html', {'form' : form})

    return render_to_response('resources/doschedule.html', {'user' : request.user, 'experiment': ex, 'expt2': expt2})

@login_required
@commit_on_success
def cancel_an_experiment(request, id):
    """
    Cancel an experiment
    """

    experiment = None
    try:
        experiment = ExperimentInstance.objects.get(subject=request.user, id=id)
    except ExperimentInstance.DoesNotExist:
        if request.user.is_staff:
            experiment = ExperimentInstance.objects.get(id=id, experiment__lab__in = request.user.labstaff.all())
        else:
            raise Http404

    experiment.when.get().cancel()

    code = ExperimentCodes(
            subject = request.user,
            experiment = experiment.experiment,
            when = datetime.now(),
            cancelled = True
            )
    code.save()

    # and now delete the experiment instance
    experiment.delete()

    from labscheduler.resources.views.details import experiment_options
    return HttpResponseRedirect(reverse(experiment_options))

@user_passes_test(lambda u: u.is_staff)
@commit_on_success
def staff_schedule_subject(request):
    """
    Allow a staff member to schedule a subject without dealing with the
    complicated rigamorale on the admin page.
    """

    if request.method == 'POST':
        form = ScheduleUserForm(request.POST)
        if form.is_valid():
            subj = form.cleaned_data['subject']
            expt = form.cleaned_data['experiment']
            room = form.cleaned_data['room']
            edate = form.cleaned_data['date']
            etime = form.cleaned_data['time']

            roomcal = RoomCal.objects.get(room=room)

            start_time = datetime.combine(edate, etime)
            start_time = start_time.replace(tzinfo=tzlocal())
            end_time = start_time + timedelta(minutes=expt.length)

            if not roomcal.has_free_time(start_time, end_time):
                return HttpResponseBadRequest("%s is not free on %s at %s" % (room.get_long_name(), edate.isoformat(), etime.isoformat()))


            ex = ExperimentInstance(
                experiment = expt,
                subject = subj,
                assigned_to = expt.lab.lab_manager,
            )
            ex.save()

            event = Event.objects.create(
                    subject = subj,
                    title = expt.pub_name,
                    exptinst = ex,
                    room = room,
                    start_time = start_time,
                    end_time = end_time
                    )
    else:
        form = ScheduleUserForm()
    return render_to_response('resources/staff_schedule_subject.html', {'user': request.user, 'form': form})

@user_passes_test(lambda u: u.is_staff)
@commit_on_success
def mark_completed(request, id):
    """
    Mark an experiment as being completed
    """

    next = None
    if request.META['HTTP_REFERER']:
        next = request.META['HTTP_REFERER']

    experiment = get_object_or_404(ExperimentInstance, id=id, experiment__lab__in = request.user.labstaff.all())

    experiment.completed = True
    experiment.save()

    return HttpResponseRedirect(next or reverse(experiment_options))

@user_passes_test(lambda u: u.is_staff)
@commit_on_success
def create_new_experiment(request):
    """
    Create an experiment without using the admin interface
    """

    if request.method == 'POST':
        form = ExperimentForm(request.POST)
        if form.is_valid():
            expt = form.save()
    else:
        form = ExperimentForm()
    return render_to_response('resources/create_new_experiment.html',{'user': request.user, 'form': form})
