/*!
 *  Lab Scheduler Survey utilities
 *  Author: Andrew Watts
 *  Copyright 2008-2009 - University of Rochester : Brain and Cognitive Sciences
 *  Licensed under the LGPLv2.1: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *  Assumes jquery.js and jqeuery-ui.js are already loaded
*/

$(document).ready(
    function() {
        $('[name=dob]').datepicker({yearRange: '-80:0', dateFormat: 'yy-mm-dd', initStatus: 'Choose your Date of Birth', showStatus: true});

        $(":input[id=id_student]").change(function() {
            var studentstate = $(this).attr("checked");
            $(':label[for=id_gradyear]').toggle(studentstate == true);
            $(':label[for=id_gradterm]').toggle(studentstate == true);
            $(':input[id=id_gradyear]').toggle(studentstate == true);
            $(':input[id=id_gradterm]').toggle(studentstate == true);
            }
        );

        $(":input[id=id_hearing_normal]").change(function() {
            var hearstate = $(this).attr("checked");
            $(":label[for='id_hearing_problems']").toggle(hearstate == false);
            $(":input[id=id_hearing_problems]").toggle(hearstate == false);
            }
        );

        $(":input[id=id_vision_normal]").change(function() {
            var visval= $(this).val();
            $(":label[for='id_vision_other']").toggle(visval == 4);
            $(":input[id=id_vision_other]").toggle(visval == 4);
            }
        );

        $(":input[id=id_student]").change();
        $(":input[id=id_hearing_normal]").attr("checked", true).change();
        $(":input[id=id_vision_normal]").change();
    }
);
