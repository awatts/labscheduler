#
# resources/forms.py
#
# Copyright (c) 2007-2008, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django import forms
from django.contrib.auth.models import User
from resources.models import Experiment, Room
from resources.fields import SubjectChoiceField, ExperimentChoiceField, RoomChoiceField
from resources.utils import gen_day_choices, date_from_string

from datetime import date

__all__ = ['ScheduleUserForm',]

class ScheduleUserForm(forms.Form):
    """
    A form to set the date and time to schedule an experiment. To be used by staff.
    """
    subject = SubjectChoiceField(queryset=User.objects.filter(is_active = True).filter(surveys__more_expts = True).order_by('last_name'))
    experiment = ExperimentChoiceField(queryset=Experiment.objects.all(), widget=forms.Select(attrs={'disabled':'disabled'}))
    room = RoomChoiceField(queryset=Room.objects.all(), widget=forms.Select(attrs={'disabled':'disabled'}))
    date = forms.TypedChoiceField(coerce=date_from_string, empty_value=date.today(), choices=gen_day_choices(14), widget=forms.Select(attrs={'disabled':'disabled'}))
    time = forms.TimeField(label=u'Start Time', help_text=u'In 24 hour time, e.g. 15:30', widget=forms.TimeInput(format='%H:%M', attrs={'disabled':'disabled'}))

    class Media:
        css = {
            'all' : ('/site_media/css/staffsched.css',)
        }

        js = ('/site_media/js/sched-tools.js', '/site_media/js/staff-sched.js',)

class ScheduleExptForm(forms.Form):
    """
    A form to set the date and time to schedule an experiment. To be used by
    subjects.
    """

    experiment = forms.IntegerField(widget=forms.HiddenInput)
    room = RoomChoiceField(queryset=Room.objects.all(), widget=forms.Select(attrs={'disabled':'disabled'}))
    date = forms.TypedChoiceField(coerce=date_from_string, empty_value=date.today(), choices=gen_day_choices(14), widget=forms.Select(attrs={'disabled':'disabled'}))
    time = forms.TimeField(label=u'Start Time', widget=forms.TimeInput(format='%H:%M', attrs={'disabled':'disabled'}))

    class Media:
        js = ('/site_media/js/sched-tools.js', '/site_media/js/exptutils.js',)

class ExperimentForm(forms.ModelForm):
    """
    A form to create an experiment based on the model
    """
    class Meta:
        model = Experiment
        #fields = ('pub_name', 'priv_name', 'desc', 'room', 'grant', 'proto',
        #          'lab', 'length', 'days', 'prereq_expts', 'conflict_expts',
        #          'suggested_pairings', 'current')
        exclude = ('equip', 'reqperson', 'requirement',)
