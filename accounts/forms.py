#
# accounts/forms.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import
from __future__ import unicode_literals

from django.contrib.auth.models import User
#from django.contrib.admin.widgets import AdminDateWidget
from django import forms
from django.contrib.localflavor.us.forms import USPhoneNumberField
from django.contrib.formtools.wizard import FormWizard
from .widgets import HispanicSelect, RaceMultiWidget, BooleanSelect, BooleanRadioSelect, RadioSideBySideRenderer, CalendarWidget
from .fields import RaceMultiValueField
from .choices import GENDER_CHOICES, VISION_CHOICES, PROFICIENTY_CHOICES, PHONE_CHOICES
from .langs import LANGS
from datetime import datetime

__all__ = ('SchedLoginForm', 'NewUserForm', 'PasswordChangeForm', 'NewSurveyForm', 'AddLangForm', 'AddPhoneForm','AddEmailForm', 'PersonalForm', 'StudentForm', 'VisionHearingForm', 'StudiesForm')

TERM_CHOICES = (
        ('', '------'),
        ('SP', 'Spring'),
        ('AU', 'Autumn'),
        ('SU', 'Summer'),
        )

class SchedLoginForm(forms.Form):
    """
    Custom login form to use email address instead of django username.
    """
    email = forms.EmailField(label="Email address")
    passwd = forms.CharField(label="Password", widget=forms.PasswordInput)


class NewUserForm(forms.Form):
    """
    A form to create a new user for the system outside of the admin interface.
    """
    first_name = forms.CharField(max_length=16)
    last_name = forms.CharField(max_length=24)
    email = forms.EmailField(label="Email address")
    passwd1 = forms.CharField(label="Password", widget=forms.PasswordInput)
    passwd2 = forms.CharField(label="Password (again)", widget=forms.PasswordInput)

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not email:
            raise forms.ValidationError('You must provide an email address.')
        if User.objects.filter(email=email).count():
            raise forms.ValidationError('This email address is already in use. If you have an account but need help, please contact us.')
        return self.cleaned_data['email']

    def clean_passwd1(self):
        if not self.cleaned_data.get('passwd1'):
            raise forms.ValidationError('You must enter a password.')
        return self.cleaned_data['passwd1']

    def clean_passwd2(self):
        if not self.cleaned_data.get('passwd2'):
            raise forms.ValidationError('You must confirm your password.')
        elif self.cleaned_data.get('passwd1') and self.cleaned_data.get('passwd2') and self.cleaned_data['passwd1'] != self.cleaned_data['passwd2']:
            raise forms.ValidationError('Please make sure your passwords match.')
        return self.cleaned_data['passwd2']

    class Media:
        css = {
            'all': ('/site_media/css/survey.css',)
        }

        js = ('/site_media/js/newuser.js',)

class PasswordChangeForm(forms.Form):
    """
    A password change form for use outside the django provided admin interface.
    If they ever write a pw form using newforms I'll probably switch to it
    instead. All methods just override superclass versions. See the docs for
    django.forms.Form for more information.
    """
    oldpasswd = forms.CharField(label="Old Password", widget=forms.PasswordInput)
    newpasswd1 = forms.CharField(label="New Password", widget=forms.PasswordInput)
    newpasswd2 = forms.CharField(label="New Password again", widget=forms.PasswordInput)

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(PasswordChangeForm, self).__init__(*args, **kwargs)

    def clean_oldpasswd(self):
        if not self.cleaned_data.get('oldpasswd'):
            raise forms.ValidationError('You must enter your old password.')
        elif not self.user.check_password(self.cleaned_data.get('oldpasswd')):
            raise forms.ValidationError('Your old password was incorrect. Please enter it again.')
        return self.cleaned_data['oldpasswd']

    def clean_newpasswd2(self):
        if self.cleaned_data.get('newpasswd1') and self.cleaned_data.get('newpasswd2') and self.cleaned_data['newpasswd1'] != self.cleaned_data['newpasswd2']:
            raise forms.ValidationError('Please make sure your passwords match.')
        return self.cleaned_data['newpasswd2']


class NewSurveyForm(forms.Form):
    """
    A form for allowing a user to enter their own information for the survey
    rather than having it entered into the admin interface for them. Only fields
    that should be user modifiable are displayed.
    """
    phone = USPhoneNumberField(label="Phone number:")
    phone_type = forms.ChoiceField(label="Type:", choices=PHONE_CHOICES)
    dob = forms.DateField(label="Date of Birth:", widget=CalendarWidget)
    gender = forms.ChoiceField(label="Sex:", choices=GENDER_CHOICES)
    hisp = forms.NullBooleanField(label="Ethnicity:", widget=HispanicSelect)
    race = RaceMultiValueField(label="Race (you may check more than one):", required=False)
    race_other = forms.CharField(label="Other race:", required=False)
    student = forms.BooleanField(label="U of R student?", required=False)
    gradyear = forms.IntegerField(label="If yes, what year do you expect to graduate?", min_value=datetime.now().year, max_value=datetime.now().year + 10, required=False)
    gradterm = forms.ChoiceField(label="And what term?", choices=TERM_CHOICES, required=False)
    hearing_normal = forms.BooleanField(label="Is your hearing normal, so far as you know?", required=False)
    hearing_problems = forms.CharField(label="If not, please describe any problems:", widget=forms.Textarea, required=False)
    vision_normal = forms.ChoiceField(label="Is your vision:", choices=VISION_CHOICES)
    vision_other = forms.CharField(label="If other, please describe:", widget=forms.Textarea, required=False)
    more_expts = forms.BooleanField(label="Are you willing to do participate in more studies in this lab?", required=False)

    class Media:
        css = {
            'all': ('/site_media/css/survey.css',)
        }

class SurveyWizard(FormWizard):
    """
    A form wizard to collect survey data in stages, rather than on one page.
    """
    def done(self, request, form_list):
        s = Survey()
        s.user = request.user
        s.datefilled = datetime.date.today()
        #TODO: fill in the rest of the survey saving logic once FormWizard
        # is working and I know what exactly you get in form_list
        s.save()
        return HttpResponseRedirect('accounts/profile/')

class AddLangForm(forms.Form):
    """
    A simple form for adding a language to a user survey.
    """
    langname = forms.ChoiceField(label="Language", choices=LANGS)
    started = forms.DateField(label="Date started")
    stopped = forms.DateField(label="Date stopped", required = False)
    proficient = forms.ChoiceField(label="Proficiency", choices=PROFICIENTY_CHOICES)
    english_country = forms.CharField(label="Non-US country where English was learned", required=False)

    def clean_started(self):
        sdate = self.cleaned_data.get('started')
        if sdate > datetime.now().date():
            raise forms.ValidationError('Start date cannot be in the future')
        return self.cleaned_data['started']

    def clean_stopped(self):
        edate = self.cleaned_data.get('stopped')
        sdate = self.cleaned_data.get('started')

        if edate:
            if not sdate:
                raise forms.ValidationError('Cannot have a stop date without a start date')
            if edate < sdate:
                raise forms.ValidationError('Stop date cannot be before start date')
            if edate > datetime.now().date():
                raise forms.ValidationError('Stop date cannot be in the future')
        return self.cleaned_data['stopped']

    def clean_english_country(self):
        country = self.cleaned_data.get('english_country')
        if country:
            name = self.cleaned_data.get('langname')
            if name.strip().upper() != 'ENGLISH':
                raise forms.ValidationError('Please only fill this field for English')
            if ' '.join(country.upper().split()) in ("US", "USA", "AMERICA", "UNITED STATES"):
                raise forms.ValidationError('Please only fill this field if you learned English outside of the United States')
        return self.cleaned_data['english_country']


class AddPhoneForm(forms.Form):
    """
    A simple form for adding a phone number to a user survey.
    """
    phone = USPhoneNumberField(label="Phone number")
    type = forms.ChoiceField(choices=PHONE_CHOICES)

class AddEmailForm(forms.Form):
    """
    A simple form for adding an email address to a user survey.
    """
    email = forms.EmailField(label="Email address")

class PersonalForm(forms.Form):
    """
    A form for updating personal information in the survey
    """
    dob = forms.DateField(label="Date of Birth:")
    gender = forms.ChoiceField(label="Sex:", choices=GENDER_CHOICES, widget=forms.RadioSelect(renderer=RadioSideBySideRenderer))
    hisp = forms.NullBooleanField(label="Ethnicity:", widget=HispanicSelect)
    race = RaceMultiValueField(label="Race (you may check more than one):", required=False)
    race_other = forms.CharField(label="Other race:", required = False)

class StudentForm(forms.Form):
    """
    A form for updating student status
    """
    student = forms.BooleanField(label="Are you a current U of R student?", widget=BooleanRadioSelect, required=False)
    gradyear = forms.IntegerField(label="If yes, when do you expect to graduate?", min_value=datetime.now().year, max_value=datetime.now().year + 10, required=False)
    gradterm = forms.ChoiceField(label="And what term?", choices=TERM_CHOICES, required=False)

    def clean_gradyear(self):
        gradyear = self.cleaned_data.get('gradyear')
        status = self.cleaned_data.get('student')
        if gradyear and not status:
            raise forms.ValidationError('If you are not a student, you cannot have a graduation year')
        if status and not gradyear:
            raise forms.ValidationError('Please provide your projected graduation year')
        return self.cleaned_data['gradyear']

    def clean_gradterm(self):
        gradterm = self.cleaned_data.get('gradterm')
        status = self.cleaned_data.get('student')
        if gradterm and not status:
            raise forms.ValidationError('If you are not a student, you cannot have a graduation term')
        if status and not gradterm:
            raise forms.ValidationError('Please provide your projected graduation term')
        return self.cleaned_data['gradterm']

class VisionHearingForm(forms.Form):
    """
    A form for updating information about vision and hearing
    """
    vision_normal = forms.ChoiceField(label="Is your vision:", choices=VISION_CHOICES, widget=forms.Select)
    vision_other = forms.CharField(label="If other, please describe:", widget=forms.Textarea, required=False)
    hearing_normal = forms.BooleanField(label="Is your hearing normal, so far as you know?", widget=BooleanRadioSelect, required=False)
    hearing_problems = forms.CharField(label="If not, please describe any problems:", widget=forms.Textarea, required=False)

    def clean_hearing_problems(self):
        hearing_normal = self.cleaned_data.get('hearing_normal')
        hearing_problems = self.cleaned_data.get('hearing_problems')
        if hearing_normal and hearing_problems:
            raise forms.ValidationError('Please also check the box indicating that you do not have normal hearing.')
        if not hearing_normal and not hearing_problems:
            raise forms.ValidationError('Please indicate your hearing problems here.')
        return self.cleaned_data['hearing_problems']

    def clean_vision_other(self):
        vision_normal = self.cleaned_data.get('vision_normal')
        vision_other = self.cleaned_data.get('vision_other')
        if vision_normal != '4' and vision_other:
            raise forms.ValidationError('Please also select \'Other\'.')
        if vision_normal == '4' and not vision_other:
            raise forms.ValidationError('Please indicate your vision problems here.')
        return self.cleaned_data['vision_other']

class StudiesForm(forms.Form):
    """
    A form for updating information about willingness to participate in studies
    """
    more_expts = forms.BooleanField(label="Are you willing to do participate in more studies in this lab?", widget=BooleanRadioSelect, required=False)
