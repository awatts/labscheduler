.. _resources-models:

=================================
LabScheduler resources models
=================================

.. module:: labscheduler.resources.models
   :synopsis: Models related to scheduling resources

Overview
========

The scheduling models can be divided into two basic groups.

The first group are the experiment models:

    * :class:`Experiment`:
        Information about the experiment in abstract
    * :class:`Requirement`:
        A restriction or prerequisite on an experiment
            
            .. warning::

                This class is 100% broken and useless at the moment.

    * :class:`ExperimentGroup`:
        A means for grouping multiple experiments for the purpose of restriction  preqrequisites based on past participation
    * :class:`ExperimentInstance`:
        A specific instance of an experiment with a time, a place, a subject, etc.
    * :class:`ExperimentCodes`:
        A means of noting the outcome of a subject's participation in an experiment.

The second group are the non-experiment models:

    * :class:`Laboratory`:
      A lab that an experiment is associated with
    * :class:`Room`
      A room where experiments can be run
    * :class:`Equipment`
      A piece of equipment necessary for an experiment

        .. warning::

            This class is completely unused currently

    * :class:`Grant`
        A grant that funds experiments

Experiment
==========

.. class:: Experiment

API Reference
-------------

Fields
~~~~~~

.. class:: Experiment

    :class:`Experiment` objects have the following fields:

    .. attribute:: Experiment.pub_name

        Required. A string (max 30 chars) of the publicly displayed name of the experiment

    .. attribute:: Experiment.priv_name

        Required. A string (max 30 chars) of an internal name for the experiment, potentially more descriptive than the public name

    .. attribute:: Experiment.desc

        Required. A free-form string describing the experiment that subjects can see

    .. attribute:: Experiment.room

        Required. A :class:`~django.db.models.ManyToManyField` of :class:`Room` objects where the experiment could be run.

    .. attribute:: Experiment.equip

        Optional. A :class:`~django.db.models.ManyToManyField` of :class:`Equipment` required for the experiment.

    .. attribute:: Experiment.grant

        Required. A :class:`~django.db.models.ManyToManyField` of :class:`Grant` that funds this experiment

    .. attribute:: Experiment.lab

        Required. The :class:`Laboratory` running this experiment

    .. attribute:: Experiment.reqperson

        Optional. A :class:`~django.db.models.ManyToManyField` of :class:`~django.contrib.auth.models.User` objects of people required for the experiment.

            .. note::

                This field is basically unused currently

    .. attribute:: Experiment.length

        Required. Length of the experiment in minutes

    .. attribute:: Experiment.days

        Required. Number of days/sessions the experiment requires

            .. note::
                
                This field is basically unused currently

    .. attribute:: Experiment.prereq_expts

        Optional. A :class:`~django.db.models.ManyToManyField` of :class:`ExperimentGroup` objects, which are prerequisites of this experiment

    .. attribute:: Experiment.conflict_expts

        Optional. A :class:`~django.db.models.ManyToManyField` of :class:`ExperimentGroup` objects, which having done would rule out doing this experiment

    .. attribute:: Experiment.suggested_pairings

        Optional. A :class:`~django.db.models.ManyToManyField` of :class:`Experiment` objects that pair nicely with this experiment for filling a time slot or in some other way

    .. attribute:: Experiment.requirement

        Optional. A :class:`~django.db.models.ManyToManyField` of :class:`Requirement` objects.

        .. warning::
        
            As noted above, this is meaningless currently

    .. attribute:: Experiment.current

        Required. A boolean of whether the experiment is currently running.

Methods
~~~~~~~

.. class:: Experiment

    .. function:: __unicode__
    
        Returns the :attr:`priv_name` as a unicode string

    .. function:: __hash__

        Returns the :func:`hash` of :attr:`priv_name` so you can have a :class:`sets.Set` (or in Python 2.6 or later, a :class:`set`) of :class:`Experiment` objects

    .. function:: rooms

        Returns a string composed of a comma separated list of rooms where the experiment can be run

    .. function:: equipment

        Returns a string composed of a comma separated list of equipment required for the experiment

    .. function:: grants

        Returns a string composed of a comma separated list of grants that fund this experiment

Requirement
===========

.. class:: Requirement

    .. warning::

        I never properly thought through how to make this work, so this just plain isn't going to right now

API Reference
-------------

Fields
~~~~~~

.. class:: Requirement

    .. attribute:: name

        Required. A string (max 50 chars) of the name of the requirement

    .. attribute:: func_name

        Required. The name of the function that checks the requirement. Function must return a boolean value

    .. attribute:: func_args

        Required. Arguments to pass to the function

Methods
~~~~~~~

.. class:: Requirement

    .. method:: __unicode__()

        Returns a string of the requirement name

    .. method:: check(user)

        Uses :func:`eval` and extended call syntax to call the function with a :class:`~django.contrib.auth.models.User` object as the first argument, and :attr:`func_args` as a list of additional argument.s

ExperimentGroup
===============

.. class:: ExperimentGroup

API Reference
-------------

Fields
~~~~~~

.. class:: ExperimentGroup

    .. attribute:: ExperimentGroup.group_name

        A string (max 50 chars) of the name of the group

    .. attribute:: ExperimentGroup.experiments

        A :class:`~django.db.models.ManyToManyField` of :class:`Experiment` objects

Methods
~~~~~~~

.. class:: ExperimentGroup

    .. method:: __unicode__

        Returns a string of the group name

ExperimentInstance
==================

.. class:: ExperimentInstance

API Reference
-------------

Fields
~~~~~~

.. class:: ExperimentInstance

    .. attribute:: ExperimentInstance.experiment

        Required. The :class:`Experiment` that this is an instance of

    .. attribute:: ExperimentInstance.subject

        Required. The :class:`~django.contrib.auth.models.User` who is the subject in the experiment

    .. attribute:: ExperimentInstance.assigned_to

        Required. The :class:`~django.contrib.auth.models.User` who is assigned to run the experiment. :attr:`~django.contrib.auth.models.User.is_staff` must be :data:`True` of this user.

    .. attribute:: ExperimentInstance.public_notes

        Optional. Free form text of notes that it would be OK for anyone to see

    .. attribute:: ExperimentInstance.private_notes

        Optional. Free form text of notes for experiment staff only

    .. attribute:: ExperimentInstance.completed

        Boolean. Has the this instance been completed? Default is :data:`False`

    :attr:`experiment` and :attr:`subject` must be unique together in the database

Methods
~~~~~~~

.. class:: ExperimentInstance

    .. method:: __unicode__

        Returns a string of :attr:`subject` and :attr:`experiment`

ExperimentCodes
===============

.. class:: ExperimentCodes

API Reference
-------------

Fields
~~~~~~

All Boolean valued fields are :data:`False` by default.

.. class:: ExperimentCodes

    .. attribute:: experiment

        Required. A :class:`Experiment` that this is associated with

    .. attribute:: when
    
        Required. A :class:`~datetime.datetime` object of when this set of experiment codes was created

    .. attribute:: ok

        Boolean. The experiment was completed satisfactorily

    .. attribute:: cancelled

        Boolean. The subject cancelled doing the experiment
    
    .. attribute:: noshow

        Boolean. The subject didn't show up for the experiment

    .. attribute:: cheated

        Boolean. The subject cheated by (for example) clicking through as fast as possible rather than doing the experiment

    .. attribute:: understand

        Boolean. The subject did not understand the task

    .. attribute:: computer

        Boolean. The subject has no computer experience

    .. attribute:: sleep

        Boolean. The subject was extremely tired and/or kept falling asleep

    .. attribute:: interrupt

        Boolean. The experiment was interrupted by an outside factor (e.g. a fire alarm)

Methods
~~~~~~~

.. class:: ExperimentCodes

    .. method:: __unicode__()

        Returns the :meth:`~Experiment.__unicode__` value of :attr:`experiment`

Laboratory
==========

.. class:: Laboratory

API Reference
-------------

Fields
~~~~~~

:attr:`~django.contrib.auth.models.User.is_staff` must be :data:`True` for all :class:`~django.contrib.auth.models.User` fields

.. class:: Laboratory

    .. attribute:: name
    
        Required. A string (max 30 chars) of the lab name

    .. attribute:: lab_owner

        Required. A :class:`~django.contrib.auth.models.User` of the lab Primary Investigator

    .. attribute:: lab_manager
        
        Required. A :class:`~django.contrib.auth.models.User` of the lab manager

    .. attribute:: lab_staff
        
        Required. A :class:`~django.db.models.ManyToManyField` of :class:`~django.contrib.auth.models.User` objects who are staff in the lab

    .. attribute:: lab_room

        Required. The :class:`Room` that is the main room of the lab

    .. attribute:: lab_phone

        Required. The North American style phone number of the lab

            .. seealso::

                :class:`django.contrib.localflavor.us.models.PhoneNumberField`

    .. attribute:: pay_per_hour

        Required. How much (in dollars) per hour does the lab pay subjects

Methods
~~~~~~~

.. class:: Laboratory

    .. method:: __unicode__()

        Returns the string of :attr:`name`

    .. method:: lab_owner_phone()

        Returns either the first work phone number listed for the lab owner or 'Unlisted'

    .. method:: lab_manager_phone()
        
        Returns either the first work phone number listed for the lab manager or 'Unlisted'

Room
====

.. class:: Room

API Reference
-------------

Fields
~~~~~~

.. class:: Room

    .. attribute:: name
    
        Required. A string (max 50 chars) of the room name

    .. attribute:: building

        Required. A string (max 30 chars) of the building the room is in

    .. attribute:: number

        Required. A string (max 30 chars) of the room number

Methods
~~~~~~~

.. class:: Room

    .. method:: __unicode__()

        Retuns the string of :attr`name`

    .. method:: get_long_name()

        Returns a string of :attr:`building` and :attr:`number`

Equipment
=========

.. note::

    Currently this class is unused

.. class:: Equipment

API Reference
-------------

Fields
~~~~~~

.. class:: Equipment

    .. attribute:: name
    
        Required. A string (max 30 chars) of the name of the piece of equipment

    .. attribute:: room

        Required. The :class:`Room` where the piece of equipment is kept

Methods
~~~~~~~

.. class:: Equipment

    .. method:: __unicode__()

        Retuns the string of :attr`name`

Grant
=====

.. class:: Grant

API Reference
-------------

Fields
~~~~~~

.. class:: Grant

    .. attribute:: name

        Required. A string (max 30 chars) of the name name of the grant

    .. attribute:: principal

        Required. A :class:`~django.db.models.ManyToManyField` of :class:`~django.contrib.auth.models.User` objects of the principal investigators on the grant. Limited to users where :attr:`~django.contrib.auth.models.User.is_staff` is :data:`True`

    .. attribute:: agency

        Required. A string (max 30 chars) of the name of the funding agency

    .. attribute:: number

        Required. A string (max 30 chars) of the grant number

    .. attribute:: startdate

        Required. A :class:`~datetime.date` object of the start date of the grant

    .. attribute:: enddate
        
        Required. A :class:`~datetime.date` object of the end date of the grant
    .. attribute:: active

        Boolean. Is the grant active? Default is :data:`False`

Methods
~~~~~~~

.. class:: Grant

    .. method:: __unicode__()

        Retuns the string of :attr:`agency` and :attr:`number`
