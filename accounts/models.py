#
# accounts/models.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, Group
from .langs import LANGS
from .choices import *
from resources.models import Laboratory
from django.contrib.localflavor.us.models import PhoneNumberField
import datetime

__all__ = ['Survey', 'Phone', 'Email', 'Language', 'DoNotSchedule']

class Survey(models.Model):
    """
    Each potential participant in experiments should fill out a survey,
    although it is optional to do so. First and last names are not included
    because they are handled by the User class
    """
    user = models.ForeignKey(User, related_name='surveys', unique=True)
    datefilled = models.DateField("Date filled out", default=datetime.date.today(), editable=False) # when form was filled out
    dob = models.DateField("Date of birth") # date of birth
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    hisp = models.NullBooleanField("Are you Hispanic or Latino?", default=False) # hispanic ethnicity y/n
    amerind = models.BooleanField("American Indian / Native Alaskan", default=False)
    afram = models.BooleanField("Black or African-American", default=False)
    pacif = models.BooleanField("Native Hawiian or Other Pacific Islander", default=False)
    asian = models.BooleanField("Asian", default=False)
    white = models.BooleanField("White", default=False)
    unk = models.BooleanField("Unknown", default=False)
    race_other = models.CharField("Other", max_length=30, blank=True) # what other race?
    student = models.BooleanField("U of R student?", default=False) # UofR student y/n
    gradyear = models.PositiveIntegerField("If so, what year you expect to graduate?", blank=True, null=True)
    gradterm = models.CharField("And what term?", max_length=2, choices=TERM_CHOICES, blank=True, default='')
    hearing_normal = models.BooleanField("Normal hearing?") # Is hearing normal?
    hearing_problems = models.TextField("If not, describe any hearing problems", blank=True, default='')
    vision_normal = models.IntegerField("Is your vision", choices=VISION_CHOICES, default=0)
    vision_other = models.TextField("If Other, please describe", max_length=50, blank=True)
    more_expts = models.BooleanField("More studies here", help_text="Are you willing to do participate in more studies in this lab?", default=False)

    def __unicode__(self):
        return self.user.__unicode__()

    def real_name(self):
        """
        @return: user's first and last name as a single string
        """
        return "%s %s" % (self.user.first_name, self.user.last_name)

    def over_18(self):
        """
        Determine if a subject is over 18 to help determine eligibility to
        participate in experiments.

        @return: True if over 18 by today, False if not.
        """
        today = datetime.date.today()
        majority = datetime.date(today.year - 18, today.month, today.day)   # 18 years before today
        return (majority > self.dob)    # true if dob is more than 18 years ago

    def primary_email(self):
        """
        @return: first email address in the database for the person
        """
        try:
            return self.emails.all()[0].email
        except IndexError:
            return None

    #def work_phone(self):
    #    """
    #    @return: first work phone number in the database for the person
    #    """
    #    try:
    #        workphone = self.phones.filter(type='work')
    #        return workphone[0].phone
    #    except IndexError:
    #        return '555-123-4567'

    def vision(self):
        """
        @return: string of the human readable version of vision_normal
        """
        return self.get_vision_normal_display()

class Phone(models.Model):
    """
    A contact phone number for a subject. Split into its own class so they can
    add an arbitrary number of them. It doesn't have its own admin interface,
    rather appearing in Survey's
    """
    survey = models.ForeignKey(Survey, related_name='phones')
    phone = PhoneNumberField("Phone number")
    type = models.CharField("Type", max_length=4, choices=PHONE_CHOICES, default='')
    shortclass = 'phone'

    def __unicode__(self):
        return self.phone

    class Meta:
        unique_together = (("survey", "phone"),)

class Email(models.Model):
    """
    A contact email address for a subject. Split into its own class so they can
    add an arbitrary number of them. It doesn't have its own admin interface,
    rather appearing in Survey's
    """
    survey = models.ForeignKey(Survey, related_name='emails')
    email = models.EmailField("Email")
    shortclass = 'email'

    def __unicode__(self):
        return self.email

    class Meta:
        unique_together = (("survey", "email"),)

class Language(models.Model):
    """
     People can speak multiple languages. The Language class won't have its own
     admin interface, and two will be shown by default in admin
     """
    survey = models.ForeignKey(Survey, related_name='languages')
    lang = models.CharField("Language", choices=LANGS, max_length=30)
    started = models.DateField()
    stopped = models.DateField(null=True, blank=True)
    proficient = models.PositiveIntegerField("Level of proficiency", choices=PROFICIENTY_CHOICES)
    english_country = models.CharField("Non-US country where English was learned", blank=True, max_length=20, help_text="Only fill in this field if this Language is English AND you learned to speak English outside of the US.")
    shortclass = 'lang'

    def __unicode__(self):
        return self.survey.user.__unicode__() + ':' + self.lang

    def proficiency(self):
        return self.get_proficient_display()

    class Meta:
        unique_together = (("survey", "lang"),)

class DoNotSchedule(models.Model):
    """
    Sometimes subjects should not be asked back to do experiments. This class
    allows for a per user, per lab entry for a Do Not Schedule list.
    """

    user = models.ForeignKey(User, related_name='do_not_schedule')
    lab = models.ForeignKey(Laboratory)
    notes = models.TextField(blank=True)

    class Meta:
        ordering = ["lab", "user"]
        verbose_name_plural = "Do not schedule"
