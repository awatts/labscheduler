import os
import sys

sys.path.append('/usr/local/django_apps/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'labscheduler.settings'

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
