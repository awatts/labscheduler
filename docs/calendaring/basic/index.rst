.. _calendaring_basic-index:

:mod:`~labscheduler.calendaring.basic` -- Basic Calendar
========================================================

.. module:: labscheduler.calendaring.basic
   :synopsis: A Basic Calendar backend
.. moduleauthor:: Andrew Watts <awatts@bcs.rochester.edu>

.. toctree::
   :maxdepth: 1
   
   models
   views
