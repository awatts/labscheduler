#
# resources/utils.py
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from resources.models import Experiment
from datetime import date, timedelta
from dateutil.parser import parse

__all__ = ['get_available', 'get_available_queryset', 'gen_day_choices', 'date_from_string',]

def get_available(user):
    """
    Gets a list of experiments available to the user. 
    """

    #TODO: See if there's a way to do this that isn't O(n^3) on Experiment.objects.all()

    experiments = Experiment.objects.filter(current=True)

    # get rid of all experiments from labs the user has been barred from
    # running experiments in
    dns = user.do_not_schedule.all()
    for d in dns:
        experiments = experiments.exclude(lab=d.lab)

    user_experiments = [e.experiment for e in user.exptinstances.all()]
    available = [x for x in experiments.order_by('lab','pub_name') if x not in user_experiments]

    # remove experiments where the subject doesn't meet the requirements
    for a in available:
        for r in a.requirement.all():
            if not r.check(user):
                available.remove(a)

        # remove experiments where the subject hasn't compeleted the prereqs
        for pre in a.prereq_expts.all():
            for p in pre.experiments.all():
                if p not in user_experiments:
                    available.remove(a)

        # remove experiments where the subject has completed conflicting expts
        for con in a.conflict_expts.all():
            for c in con.experiments.all():
                if c in user_experiments:
                    available.remove(a)

    return available

def get_available_queryset(user):
    """
    Like get_available, but returning a QuerySet. Also leaves out the
    meaningless requirements check
    """
    
    #TODO: See if there's a way to do this that isn't O(n^3) on Experiment.objects.all()

    experiments = Experiment.objects.filter(current=True)
    user_experiments = [e.experiment for e in user.exptinstances.all()]

    # get rid of all experiments from labs the user has been barred from
    # running experiments in
    dns = user.do_not_schedule.all()
    for d in dns:
        experiments = experiments.exclude(lab=d.lab)

    # remove experiments the user has already done
    for u in user_experiments:
        if u in experiments:
            experiments = experiments.exclude(id=u.id)

    for e in experiments:

        # remove experiments where the subject hasn't compeleted the prereqs
        for pre in e.prereq_expts.all():
            for p in pre.experiments.all():
                if p not in user_experiments:
                    experiments = experiments.exclude(id=p.id)

        # remove experiments where the subject has completed conflicting expts
        for con in e.conflict_expts.all():
            for c in con.experiments.all():
                if c in user_experiments:
                    experiments = experiments.exclude(id=c.id)

    return experiments.order_by('lab','pub_name')

def gen_day_choices(count):
    d = date.today()
    dl = [d,]
    for i in range(count):
        d += timedelta(days=1)
        dl.append(d)
    return [("","---------")] + zip([d.isoformat() for d in dl],[d.strftime("%A %d. %B %Y") for d in dl])

def date_from_string(datestring):
    d = parse(datestring)
    return d.date()
