#
# accounts/views.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import
from __future__ import unicode_literals
from __future__ import print_function

from django.shortcuts import render_to_response
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.template.defaultfilters import date
from django.db import IntegrityError
from utils import ErrorsToDict
from .models import Survey, Email, Phone, Language
from .forms import *
from .choices import *
from json_response import JsonResponse
import datetime

__all__ = ('index', 'login', 'new_user', 'pwchange', 'initial_survey',
           'add_object', 'del_object', 'manage_personal', 'manage_student',
           'manage_hearing_vision', 'manage_studies', 'profile',)

def index(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse(profile))
    else:
        return HttpResponseRedirect(reverse(new_user))

def login(request):
    """
    Custom login view. Uses email address and password to login. Django username
    is irrelevant.
    """

    next = reverse(profile)

    if request.method == 'POST':
        if 'next' in request.POST:
            next = request.POST['next']

        form = SchedLoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            passwd = form.cleaned_data['passwd']

            # Light security check -- make sure redirect_to isn't garbage.
            # from original django.contrib.auth.views.login
            if not next or '//' in next or ' ' in next:
                from django.conf import settings
                next = settings.LOGIN_REDIRECT_URL

            user = None
            try:
                user = User.objects.get(email=email)
            except User.DoesNotExist:
                return render_to_response('accounts/login.html', {'form': form, 'errors': True, 'next': next})

            authuser = authenticate(username=user.username, password=form.cleaned_data['passwd'])
            if authuser is not None:
                auth_login(request, authuser)
                return HttpResponseRedirect(next)
            else:
                return render_to_response('accounts/login.html', {'form':form, 'errors': True, 'next': next })
    else:
        if request.user.is_authenticated():
            auth_logout(request)
        form = SchedLoginForm()

    return render_to_response('accounts/login.html', {'form': form, 'next': next})

def logout(request):
    from django.contrib.auth.views import logout as django_logout
    return django_logout(request, next_page="/")

def check_email_address(request):
    """
    AJAX method to check the db to see if the email address already exists
    """
    if not request.is_ajax():
        return HttpResponseBadRequest("This function is meant to be called via AJAX only.")

    reply = ''
    email = request.GET.get('email')

    if email:
        if User.objects.filter(email=email).count():
            reply = '<li>This email address is already in use. If you have an account but need help, please contact us.</li>'
    return HttpResponse(reply)

def new_user(request):
    """
    Logic for creating a new user. They provide their first and last names, and
    a password. A username is generated from their first initial and last name,
    with the final letter being replaced by a number in the instance that the
    username already exists.
    """
    if request.user.is_authenticated():
        #return HttpResponseRedirect('/accounts/logout/?next=%s' % request.path)
        from django.contrib.auth.views import logout
        return logout(request, next_page=request.path)

    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            fname = form.cleaned_data['first_name']
            lname = form.cleaned_data['last_name']
            uname = ''.join((fname[:1], lname[:7])).lower()
            likenames = User.objects.filter(username__istartswith=uname[:7])
            if likenames: #add a number if username is taken
                uname = uname[:7] + str(len(likenames))
            user = User.objects.create_user(uname, form.cleaned_data['email'], form.cleaned_data['passwd1'])
            user.first_name = fname
            user.last_name = lname
            user.save()

            authuser = authenticate(username=user.username, password=form.cleaned_data['passwd1'])
            if authuser is not None:
                auth_login(request, authuser)
                return HttpResponseRedirect(reverse(initial_survey))
            else:
                return HttpResponseBadRequest('Something went awry in account creation.')

    else:
        form = NewUserForm()
    return render_to_response('accounts/create.html', {'form': form})

@login_required
def pwchange(request):
    """
    Logic for handling a password change. Only necessary until the django
    people write a newforms password change form and function.

    @return: redirect to login page if not logged in, the change form page initially, and the profile page on success.
    """
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)

        #decide where to go after changing the password
        next = '/accounts/profile/'
        if 'HTTP_REFERER' in request.META:
            next = request.META['HTTP_REFERER']

        if form.is_valid():
            request.user.set_password(form.cleaned_data['newpasswd2'])
            request.user.save()
            return HttpResponseRedirect('%s' % next)
        elif request.POST['change'] == "Cancel":
            return HttpResponseRedirect('%s' % next)
    else:
        form = PasswordChangeForm(request.user)
    return render_to_response('accounts/password_change.html', {'form': form})

@login_required
def initial_survey(request):
    """
    Logic for handling a new user survey.

    @return: redirect to login page if not logged in, the survey form initially, and the profile page on success.
    """
    if Survey.objects.filter(user=request.user):
        return HttpResponseRedirect(reverse(profile))

    if request.method == 'POST':
        form = NewSurveyForm(request.POST)
        if form.is_valid():
            s = Survey()
            s.user = request.user
            s.datefilled = datetime.date.today()
            s.dob = form.cleaned_data['dob']
            s.gender = form.cleaned_data['gender']
            s.hisp = form.cleaned_data['hisp']
            s.amerind, s.afram, s.pacif, s.asian, s.white, s.unk = form.cleaned_data['race']
            s.race_other = form.cleaned_data['race_other']
            s.student = form.cleaned_data['student']
            s.gradyear = form.cleaned_data['gradyear']
            s.gradterm = form.cleaned_data['gradterm']
            s.hearing_normal = form.cleaned_data['hearing_normal']
            s.hearing_problems = form.cleaned_data['hearing_problems']
            s.vision_normal = form.cleaned_data['vision_normal']
            s.vision_other = form.cleaned_data['vision_other']
            s.more_expts = form.cleaned_data['more_expts']
            s.save()

            e = Email()
            e.survey = s
            e.email = request.user.email
            e.save()

            p = Phone()
            p.survey = s
            p.phone = form.cleaned_data['phone']
            p.type = form.cleaned_data['phone_type']
            p.save()

            return HttpResponseRedirect(reverse(profile))
    else:
        form = NewSurveyForm()
    return render_to_response('accounts/survey.html', {'user': request.user, 'form': form})

@login_required
def add_object(request, objtype):
    """
    Abstracted logic for adding an object associated with the logged in user. On
    success it returns the saved object JSON encoded. If the user has no survey
    it redirects them to create one. If an invalid object type is passed in it
    returns an HTTP Response Bad Request.
    """

    # Get the user's survey, or redirect them to do a survey first
    s = None
    try:
        s = Survey.objects.get(user=request.user)
    except Survey.DoesNotExist:
        return HttpResponseRedirect(reverse(initial_survey))

    formclass = None
    try:
        formclass = {'phone': AddPhoneForm, 'email': AddEmailForm, 'lang': AddLangForm}[objtype]
    except KeyError:
        return HttpResponseBadRequest("Error: Attempt to add a non-existant type.")

    if request.method == 'POST':
        if not request.is_ajax():
            return HttpResponseBadRequest("This function is meant to be called via AJAX only.")

        form = formclass(request.POST)
        if not form.is_valid():
            errors = ErrorsToDict(form.errors)
            return JsonResponse({'errors': errors})
        else:
            if objtype == 'phone':
                p = Phone()
                p.survey = s
                p.phone = form.cleaned_data['phone']
                p.type = form.cleaned_data['type']
                try:
                    p.save()
                    return JsonResponse({'phone': p.phone, 'type': p.type, 'id': p.id})
                except IntegrityError:
                    return JsonResponse({
                        'errors': {
                            'phone': ['Phone number already exists']
                            }
                        }
                    )
            elif objtype == 'email':
                e = Email()
                e.survey = s
                e.email = form.cleaned_data['email']
                try:
                    e.save()
                    return JsonResponse({'uname': request.user.get_full_name(), 'email': e.email, 'id': e.id})
                except IntegrityError:
                    return JsonResponse({
                        'errors': {
                            'email': ['Email address already exists']
                            }
                        }
                    )
            elif objtype == 'lang':
                l = Language()
                l.survey = s
                l.lang = form.cleaned_data['langname']
                l.started = form.cleaned_data['started']
                l.stopped = form.cleaned_data['stopped']
                l.proficient = int(form.cleaned_data['proficient'])
                l.english_country = form.cleaned_data['english_country']
                l.save()
                return JsonResponse({'lang': l.lang, 'started': date(l.started, "F Y"),
                    'stopped': date(l.stopped, "F Y"), 'proficient': l.get_proficient_display(),
                    'id': l.id})
    else:
        form = formclass()
    return render_to_response('accounts/add_' + objtype + '.html', {'form': form, 'obj': objtype})

@login_required
def del_object(request, objtype, id):
    """
    Abstracted logic for deleting an object associated with the logged in user.
    If the user has no survey it redirects them to create one. If an invalid
    object type is passed in it returns an HTTP Response Bad Request.
    """

    if not request.is_ajax():
        return HttpResponseBadRequest("This function is meant to be called via AJAX only.")
    # Get the user's survey, or redirect them to do a survey first
    s = None
    try:
        s = Survey.objects.get(user=request.user)
    except Survey.DoesNotExist:
        return HttpResponseRedirect(reverse(initial_survey))

    try:
        #TODO: Some sort of check should be here, so if they try to delete the
        # last phone number or email it won't let them. Also when penultimate
        # email is deleted, set User email to that
        {'phone': Phone, 'email': Email, 'lang': Language}[objtype].objects.filter(id=id, survey=s).delete()
        return JsonResponse({'type': objtype, 'id': id})
    except (KeyError, Phone.DoesNotExist. Email.DoesNotExist, Language.DoesNotExist):
        return HttpResponseBadRequest("Error: Attempt to delete a non-existant object.")

@login_required
def manage_personal(request):
    """
    change the values of the personal data portion of the survey
    """

    # Get the user's survey, or error out, as this should only be called
    # from the profile page
    s = None
    try:
        s = Survey.objects.get(user=request.user)
    except Survey.DoesNotExist:
        return HttpResponseBadRequest()

    if request.method == 'POST':
        form = PersonalForm(request.POST)
        if form.is_valid():
            s.dob = form.cleaned_data['dob']
            s.gender = form.cleaned_data['gender']
            s.hisp = form.cleaned_data['hisp']
            s.amerind, s.afram, s.pacif, s.asian, s.white, s.unk = form.cleaned_data['race']
            s.race_other = form.cleaned_data['race_other']
            s.save()

            race = {'amerind': s.amerind, 'afram': s.afram, 'pacif': s.pacif,
                    'asian': s.asian, 'white': s.white, 'unk': s.unk,
                    'other': s.race_other}

            return render_to_response('accounts/personal.html',
                {'dob': s.dob, 'gender': dict(GENDER_CHOICES)[s.gender],
                'hisp': s.hisp, 'race': race})
    else:
        race = [{True: val[0], False: ''}[val[1]] for val in (('1', s.amerind),
         ('2', s.afram), ('3', s.pacif), ('4', s.asian), ('5', s.white), ('6', s.unk))]

        data = {'dob': s.dob,
                'gender': s.gender,
                'hisp': s.hisp,
                'race': race,
                'race_other': s.race_other
                }
        form = PersonalForm(data=data)
    return render_to_response('accounts/personal_form.html', {'user': request.user, 'form': form})

@login_required
def manage_student(request):
    """
    change the values for the student status portion of the survey
    """

    if not request.is_ajax():
        return HttpResponseBadRequest("This function is meant to be called via AJAX only.")

    # Get the user's survey, or error out, as this should only be called
    # from the profile page
    s = None
    try:
        s = Survey.objects.get(user=request.user)
    except Survey.DoesNotExist:
        return HttpResponseBadRequest()

    if request.method == 'POST':
        for key in request.POST:
            print(key + ' ' + request.POST[key])

        form = StudentForm(request.POST)

        if form.is_valid():
            s.student = form.cleaned_data['student']
            s.gradyear = form.cleaned_data['gradyear']
            s.gradterm = form.cleaned_data['gradterm']
            s.save()

            return render_to_response('accounts/student_status.html',
                {'student': s.student, 'gradyear': ' '.join((s.gradterm, str(s.gradyear)))})
    else:
        student = {True: '1', False: '2'}[s.student]
        gradyear = s.gradyear
        if gradyear:
            gradyear = str(gradyear)
        data = {'student': student,
                'gradyear': gradyear,
                'gradterm': s.gradterm
                }
        form = StudentForm(data=data)
    return render_to_response('accounts/student_form.html', {'user': request.user, 'form': form})

@login_required
def manage_hearing_vision(request):
    """
    change the values for the student status portion of the survey
    """

    if not request.is_ajax():
        return HttpResponseBadRequest("This function is meant to be called via AJAX only.")
    # Get the user's survey, or error out, as this should only be called
    # from the profile page
    s = None
    try:
        s = Survey.objects.get(user=request.user)
    except Survey.DoesNotExist:
        return HttpResponseBadRequest()

    if request.method == 'POST':
        form = VisionHearingForm(request.POST)
        if form.is_valid():
            s.hearing_normal = form.cleaned_data['hearing_normal']
            s.hearing_problems = form.cleaned_data['hearing_problems']
            s.vision_normal = form.cleaned_data['vision_normal']
            s.vision_other = form.cleaned_data['vision_other']
            s.save()

            vision = dict(VISION_CHOICES)[int(s.vision_normal)]

            return render_to_response('accounts/visionhearing.html',
                {'vision': vision, 'vision_other': s.vision_other,
                 'hearing': {True: '1', False: '2'}[s.hearing_normal],
                 'hearing_other': s.hearing_problems})
    else:
        data = {
                'hearing_normal': {True: '1', False: '2'}[s.hearing_normal],
                'hearing_problems': s.hearing_problems,
                'vision_normal': str(s.vision_normal),
                'vision_other': s.vision_other
                }
        form = VisionHearingForm(data=data)
    return render_to_response('accounts/visionhearing_form.html',
        {'user': request.user, 'form': form})

@login_required
def manage_studies(request):
    """
    change the values for the subject participation portion of the survey
    """

    if not request.is_ajax():
        return HttpResponseBadRequest("This function is meant to be called via AJAX only.")
    # Get the user's survey, or error out, as this should only be called
    # from the profile page
    s = None
    try:
        s = Survey.objects.get(user=request.user)
    except Survey.DoesNotExist:
        return HttpResponseBadRequest()

    if request.method == 'POST':
        form = StudiesForm(request.POST)
        if form.is_valid():
            s.more_expts = form.cleaned_data['more_expts']
            s.save()

            return render_to_response('accounts/studies.html', {'studies': s.more_expts})
    else:
        more_expts = {True: '1', False: '2'}[s.more_expts]
        data = {
                'more_expts': more_expts,
                }
        form = StudiesForm(data=data)
    return render_to_response('accounts/studies_form.html',
        {'user': request.user, 'form': form})


@login_required
def profile(request):
    """
    Display the Survey information for a user in order for them to verify that
    it is correct and allow them to manage objects that can occur multiple
    times, e.g. phone numbers or email addresses.

    @return: redirect to login page if not logged in, the survey page if a survey doesn't already exist for the user, or the profile page.
    """
    try:
        survey = Survey.objects.get(user=request.user)
    except Survey.DoesNotExist: # there's no survey for this user
        return HttpResponseRedirect(reverse(initial_survey))

    emails = survey.emails.all().order_by('id')
    phones = survey.phones.all().order_by('id')
    dob = survey.dob
    gender = survey.get_gender_display()
    hisp = survey.hisp
    race = {'amerind': survey.amerind, 'afram': survey.afram, 'pacif': survey.pacif,
            'asian': survey.asian, 'white': survey.white, 'unk': survey.unk,
            'other': survey.race_other}
    student = survey.student
    gradyear = ' '.join((survey.gradterm, str(survey.gradyear)))
    vision = survey.get_vision_normal_display()
    vision_other = survey.vision_other
    hearing = survey.hearing_normal
    hearing_other = survey.hearing_problems
    languages = survey.languages.all()
    studies = survey.more_expts

    return render_to_response('accounts/profile.html', {'user' : request.user, 'emails': emails, 'phones': phones, 'dob' : dob, 'gender' : gender, 'hisp': hisp, 'race':race, 'student': student, 'gradyear': gradyear, 'vision': vision, 'vision_other': vision_other, 'hearing': hearing, 'hearing_other': hearing_other, 'languages': languages, 'studies': studies})
