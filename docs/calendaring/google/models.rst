.. _calendaring_google-models:

======================
Google Calendar Models
======================

.. module:: labscheduler.calendaring.google.models
   :synopsis: Models related to connecting to google calendar

Overview
========
The following models are provided:
    * :class:`GoogleCredentials`

      Connects a :class:`~django.contrib.auth.models.User` with a Google `AuthSub <http://code.google.com/apis/accounts/docs/AuthSub.html>`_ token.
    
    * :class:`GoogleCalendar`

      Connects a :class:`~django.contrib.auth.models.User` with a Google Calendar feed.
    
    * :class:`RoomGCal`
      
      Connects a :class:`~django.contrib.auth.models.User` and a :class:`~labscheduler.resources.models.Room` with a Google Calendar feed.
    
    * :class:`GoogleEvent`

      Connects an :class:`~labscheduler.resources.models.ExperimentInstance` with a :class:`~django.contrib.auth.models.User` and an event from a Google Calendar feed. Managed with :class:`GEventManager`.

as well as the following helper function
    * :func:`cal_service_for_user`

Helper Functions
================

.. function:: cal_service_for_user(user)
   
   :param user: A :class:`~django.contrib.auth.models.User`

   Given a user, authenticates to Google Calendar with their credentials and returns a :class:`gdata.calendar.service.CalendarService` object, which allows you to interact with their calendar feeds.

GoogleCredentials
=================

.. class:: GoogleCredentials

API Reference
-------------

Fields
~~~~~~
.. attribute:: GoogleCredentials.user

   The :class:`~django.contrib.auth.models.User` whose credentials these are.

.. attribute:: GoogleCredentials.token

   A string of the AuthSub token returned by Gooogle. Max 64 chars.

.. attribute:: GoogleCredentials.token_valid

   A Boolean of whether this is a valid AuthSub token. Intially :data:`False`. Once you have upgraded from a session token to a permanent token, this should be set to :data:`True`. If the token ever fails it should be reset to :data:`False` so the system knows to try to get a new token.

Methods
~~~~~~~

None

GoogleCalendar
==============

.. class:: GoogleCalendar

API Reference
-------------

Fields
~~~~~~

.. attribute:: GoogleCalendar.user

   The :class:`~django.contrib.auth.models.User` whose credentials these are.

.. attribute:: GoogleCalendar.name

   String of the name of the calendar feed. Max 96 chars.

.. attribute:: GoogleCalendar.feed

   String of the Google Calendar feed. Max 256 chars. Usually either an email address (for a primary calendar) or a 26 digit base62 (i.e. :regexp:`[A-Za-z0-9]{26}`) string plus ``@group.calendar.google.com`` (for secondary calendars). 

Methods
~~~~~~~

.. method:: GoogleCalendar.user_display_name

   Returns a string of the :attr:`user`'s full name and email.

RoomGCal
========

.. class:: RoomGCal

API Reference
-------------

Fields
~~~~~~
This is a subclass of :class:`GoogleCalendar` that adds the following field:

.. attribute:: room

   The :class:`~labscheduler.resources.models.Room` that is connected to this calendar.

Methods
~~~~~~~

.. method:: RoomGCal.__unicode__

   Returs a string of "RoomGCal:" plus the :attr:`room`'s name.

.. method:: RoomGCal.has_free_time(start_time, end_time)

   :param start_time: A :class:`datetime.datetime` object of the start of the time period to check

   :param end_time: A :class:`datetime.datetime` object of the end of the time period to check

   Returns :data:`False` if there are any busy events in the specified time period or :data:`True` if there are none.

.. method:: RoomGCal.get_busy_times(day)

   :param day: A :class:`datetime.date` object for the day you want to check for busy times

   Returns a list of dictionaries in the form::
    
    {'start' : 'YYYY-MM-DDTHH:MM:SS.mmmmmm+HH:MM', 'stop' : 'YYYY-MM-DDTHH:MM:SS.mmmmmm+HH:MM'}

   where the values are ISO 8601 timestamps including timezone offset info.

GoogleEvent
===========

.. class:: GoogleEvent

API Reference
-------------

Fields
~~~~~~

.. attribute:: subject
   
   The :class:`~django.contrib.auth.models.User` associated with the event.

.. attribute:: room
   
   The :class:`~labscheduler.resources.models.Room` associated with the event.

.. attribute:: exptinstance
   
   The :class:`~labscheduler.resources.models.ExperimentInstance` associated with the event.

.. attribute:: html_link

   The Google provided URL to a viewable page of the event.

.. attribute:: edit_link

  The Google provided URL to a page where you can get the ATOM version of the event and where you push the updated event.

Methods
~~~~~~~

.. method:: room_name

   Returns a string of :attr:`room`'s name.

.. method:: start_time

   Returns a :class:`datetime.datetime` object for the start time of the event, which is fetched from Google.

.. method:: endtime
   
   Returns a :class:`datetime.datetime` object for the end time of the event, which is fetched from Google.

.. method:: update_time(newstart, newend)

   :param newstart: A :class:`datetime.datetime` object of the new start time.
   :param newend: A :class:`datetime.datetime` object of the new end time.

   Updates the event on Google Calendar.
   
   .. note:: 
   
     Does not return a value.

.. method:: cancel

   Deletes the event on Google Calendar, and then deletes the :class:`GoogleEvent` object itself.

GEventManager
=============

.. class:: GEventManager

This class subclasses :class:`django.db.models.Manager`, but only overrides the :meth:`~django.db.models.Manager.create` method.

API Reference
-------------

Fields
~~~~~~

None

Methods
~~~~~~~

.. method:: create(subject=AnonymousUser(), title="", exptinst=None, room = None, start_time=None, end_time=None, **kwargs)

   :param subject: A :class:`~django.contrib.auth.models.User` (or subclass) object. Default is an :class:`~django.contrib.auth.models.AnonymousUser`.

   :param title: A string of the title of the event

   :param expinst: A string of the :attr:`labscheduler.resources.models.ExperimentInstance.id`

   :param room: A :class:`~labscheduler.resources.models.Room` object
   :param start_time: A :class:`datetime.datetime` object
   :param end_time: A :class:`datetime.datetime` object

   Creates a new :class:`GoogleEvent` and adds it to the appropriate google calendar. Be sure the times include timezone data or google with make it UTC.
