#
# resources/fields.py
#
# Copyright (c) 2007-2008, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.forms import ModelChoiceField

__all__ = ['SubjectChoiceField', 'ExperimentChoiceField', 'RoomChoiceField',]

class SubjectChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.last_name + ', ' + obj.first_name + ' (' + obj.email + ')'

class ExperimentChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.lab.name + " : " + obj.pub_name

class RoomChoiceField(ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name + " (" + obj.building + " " + obj.number + ")"
