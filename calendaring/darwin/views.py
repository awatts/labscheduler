#
# darwin_calendar/views.py
#
# Author: Andrew Watts
# Created: Mon 16-Jun-2008 11:43 EDT
#
# Copyright (c) 2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse

__all__ = ['index',]

def index(request):
    """
    Default action to take if the user goes to the calendar top level path
    """
    pass


def get_busy_times(principal, startdate=None, enddate=None):
    pass
