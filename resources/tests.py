#
# tests.py
#
# Author: Andrew Watts
# Created: Thu 13-Dec-2007 16:45 EST
# Modified: Wed 19-Dec-2007 11:30 EST
#
# Copyright (c) 2007, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from labscheduler.accounts.models import Survey, Language
import datetime

def over_18(user):
    """
    Determine if a subject is over 18 to help determine eligibility to
    participate in experiments.

    @return: True if over 18 by today, False if not over 18 or if user has no
    survey.
    """

    try:
        survey = Survey.objects.get(user = user)
    except:
        return False

    today = datetime.date.today()
    majority = datetime.date(today.year - 18, today.month, today.day)   # 18 years before today
    return (majority > survey.dob)    # true if dob is more than 18 years ago

def native_speaker(user, langname):
    """
    Test for native speaker of a language to aid in determining eligibility
    for experiments.

    @return: True if a native speaker of the language, else False
    """
    try:
        survey = Survey.objects.get(user = user)
        language = Language.objects.get(survey=survey, lang__iexact=langname)
    except:
        return False

    if language.proficient == 4:
        return True
    return False

def native_US_English(user):
    """
    Test for native speaker of US English to aid in determining eligibility
    for experiments.

    @return: True if native speaker and country where English was learned
    isn't specified, else False.
    """
    try:
        survey = Survey.objects.get(user = user)
        english = Language.objects.get(survey=survey, lang__iexact=u"English")
    except:
        return False

    # check for native proficiency and that the english country field is blank
    if english.proficient == 4 and not english.english_country:
        return True
    return False
