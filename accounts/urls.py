from __future__ import absolute_import
from django.conf.urls.defaults import *
#from .forms import PersonalForm, StudentForm, VisionHearingForm, AddLangForm, StudiesForm, SurveyWizard

#urlpatterns = patterns('',
    #(r'survey/$', SurveyWizard([PersonalForm, StudentForm, VisionHearingForm, AddLangForm, StudiesForm])),
#)

urlpatterns = patterns('accounts.views',
    (r'^$', 'index'),
    (r'login/$', 'login'),
    (r'logout/$', 'logout'),
    (r'create/$', 'new_user'),
    (r'check/', 'check_email_address'),
    (r'profile/$', 'profile'),
    (r'password_change/$', 'pwchange'),
    (r'survey/$', 'initial_survey'),
    (r'add/(?P<objtype>\w+)/$', 'add_object'),
    (r'del/(?P<objtype>\w+)/(?P<id>\d+)/$', 'del_object'),
    (r'survey/personal/$', 'manage_personal'),
    (r'survey/student/$', 'manage_student'),
    (r'survey/hearingvision/$', 'manage_hearing_vision'),
    (r'survey/studies/$', 'manage_studies'),
)
