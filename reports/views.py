#
# views.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is provided under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse
from django.template import loader, Context
from accounts.models import Survey, Language, DoNotSchedule
from accounts.langs import LANGS
from resources.models import ExperimentInstance

__all__ = ('language_summary', 'new_enrollment', 'willing_subjects', 'dns_list', 'expt_codes_and_count', )

def index(request):
    return HttpResponseRedirect(reverse(language_summary))

@user_passes_test(lambda u: u.is_staff)
def language_summary(request):
    """
    Generates a summary of how many people in the subject pool speak each
    language
    """

    #TODO: change so we *only* get languages with a count of 1 or greater

    # start by getting the total count of people with surveys and the
    # total of languages
    total_users = Survey.objects.all().count()
    total_langs = Language.objects.all().count()

    # next get the count of people who claim to speak each language at all and
    # how many people say each language is a native language
    languages = [lang[0] for lang in LANGS]

    lang_counts = [dict((('lang', lang), ('count', Language.objects.filter(lang=lang).count()))) for lang in languages]

    native_counts = [dict((('lang', lang), ('count', Language.objects.filter(lang=lang, proficient__exact=4).count()))) for lang in languages]

    near_counts = [dict((('lang', lang), ('count', Language.objects.filter(lang=lang, proficient__exact=3).count()))) for lang in languages]

    competent_counts = [dict((('lang', lang), ('count', Language.objects.filter(lang=lang, proficient__exact=2).count()))) for lang in languages]

    novice_counts = [dict((('lang', lang), ('count', Language.objects.filter(lang=lang, proficient__exact=1).count()))) for lang in languages]

    return render_to_response('reports/lang_summary.html', {'user' : request.user, 'total_users' : total_users, 'total_langs' : total_langs, 'lang_counts' : lang_counts, 'native_counts' : native_counts, 'near_counts' : near_counts, 'competent_counts' : competent_counts, 'novice_counts' : novice_counts})


@user_passes_test(lambda u: u.is_staff)
def new_enrollment(request):
    """
    Gets the count and names of subjects who have enrolled this calendar year
    Possible future change: take a year as a parameter and get the subjects who
    enrolled that year
    """

    #TODO: Fill in the rest

    return render_to_response('reports/new_enrollment.html', {'user' : request.user})

@user_passes_test(lambda u: u.is_staff)
def missed_experiments(request):
    """
    Finds all experiment appointments that have passed without having been
    marked as completed, giving the option to mark or reschedule.
    """

    incomplete = ExperimentInstance.filter(completed = False).order_by('sched_date')

    #TODO: Fill the rest in

    return render_to_response('reports/missed_experiments.html', {'user' : request.user})

@user_passes_test(lambda u: u.is_staff)
def willing_subjects(request, type='html'):
    """
    Generates a list of subjects willing to participate in future experiments
    """

    willing = Survey.objects.filter(more_expts = True).order_by('user__last_name')

    ulist = ["%s <%s>" % (w.real_name(), w.primary_email()) for w in willing]

    if type == 'text':
        response = HttpResponse(mimetype='text/plain')
        response['Content-Disposition'] = 'attachment; filename=willing_subjects.txt'
        t = loader.get_template('reports/willing_subjects.txt')
        c = Context({'ulist': ulist})
        response.write(t.render(c))
        return response

    return render_to_response('reports/willing_subjects.html', {'user' :request.user, 'ulist' : ulist })

@user_passes_test(lambda u: u.is_staff)
def dns_list(request):
    """
    Generate a list of subjects who are not to be scheduled for any future
    experiments in the staff member's lab.
    """

    dns = DoNotSchedule.objects.filter(lab__in = request.user.labstaff.all())

    return render_to_response('reports/noschedule.html', {'user' :request.user, 'dlist' : dns })

@user_passes_test(lambda u: u.is_staff)
def expt_codes_and_count(request):
    """
    Generate a list of experiment instances including the ExperimentCodes and
    count of times the experiment has been done.
    """

    expts = Experiment.objects.filter(lab__in = request.user.labstaff.all())

    for e in expts:
        codes = ExperimentCodes.objects.filter(experiment = e)
