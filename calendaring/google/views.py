#
# calendaring/google/views.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

import re
from datetime import date, timedelta
import datetime
from dateutil.parser import parse
from dateutil.tz import tzlocal
from urllib import unquote

import gdata.service
from gdata.service import NonAuthSubToken
import gdata.calendar.service
from gdata.calendar.service import RequestError
import atom

from django.shortcuts import render_to_response
from django.core.urlresolvers import reverse
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET
from django.conf import settings

from .models import GoogleCredentials, GoogleCalendar, RoomGCal
from .utils import cal_service_for_user
from resources.models import Experiment
from json_response import JsonResponse

__all__ = ['index', 'authsub', 'view_calendars',]

@login_required
def index(request):
    """
    Index page for google calendars views
    """

    if not request.user.is_staff:
        return HttpResponseRedirect(reverse(upcoming))

    calendar_service = cal_service_for_user(request.user)
    if calendar_service:
        return HttpResponseRedirect(reverse(view_calendars))
    else:
        next = settings.HOST_ROOT + reverse(authsub)
        scope = 'http://www.google.com/calendar/feeds/'
        secure = False
        session = True
        calendar_service = gdata.calendar.service.CalendarService()
        auth_sub_url = calendar_service.GenerateAuthSubURL(next, scope, secure, session)

        return render_to_response('google_calendar/index.html', {'user': request.user, 'auth_sub_url': auth_sub_url})

@login_required
def upcoming(request):
    """
    Show upcoming scheduled events
    """

    experiments = None
    try:
        experiments = request.user.exptinstances.filter(when__start_time__gte=datetime.date.today())
    except ExperimentInstance.DoesNotExist:
        pass

    return render_to_response('google_calendar/eventview.html', {'user': request.user, 'experiments': experiments})


@login_required
def authsub(request):
    """
    Upgrade a google authsub single-use token to a session token and show the
    user the result.
    """

    #TODO: make a more clear error page

    if not 'token' in request.GET:
        try:
            if request.user.gcreds.get().token_valid:
                return HttpResponseRedirect(reverse(view_calendars))
        except GoogleCredentials.DoesNotExist:
            pass # fall through to the 400 error
        return HttpResponseBadRequest('Failed attempt to upgrade token.')
    else:
        # first create a GoogleCredentials object for the user
        # if they don't already have one
        creds = None
        try:
            creds = request.user.gcreds.get()
            creds.token = request.GET['token']
            creds.save()
        except GoogleCredentials.DoesNotExist:
            creds = GoogleCredentials()
            creds.user = request.user
            creds.token = request.GET['token']
            creds.save()
        
        # next try to get a session token (which is actually permanent,
        # confusingly enough)
        calendar_service = gdata.calendar.service.CalendarService()
        # can't set calendar_service.auth_token directly anymore
        # must use setter method
        calendar_service.SetAuthSubToken(creds.token)
        try:
            calendar_service.UpgradeToSessionToken()
        except NonAuthSubToken:
            return HttpResponseBadRequest('Failed attempt to upgrade token.')
        creds.token = calendar_service.GetAuthSubToken()
        creds.token_valid = True
        creds.save()

        return HttpResponseRedirect(reverse(view_calendars))

@login_required
def view_calendars(request):
    """
    Show a list of the user's calendars
    """
    calendar_service = cal_service_for_user(request.user)

    try:
        feed = calendar_service.GetOwnCalendarsFeed()

        feed_title = feed.title.text

        cal_dict = [dict([('title', a_calendar.title.text), ('link', a_calendar.GetAlternateLink().href), ('color', a_calendar.color.value)]) for a_calendar in feed.entry]

        goog = re.compile(r'^http://www\.google\.com/calendar/feeds/(.*)/private/full$')
        for c in cal_dict:
            title = c['title']
            feed = unquote(goog.match(c['link']).groups()[0])
            color = c['color']
            try:
                cal = GoogleCalendar.objects.get(user=request.user, feed=feed)
            except GoogleCalendar.DoesNotExist:
                cal = GoogleCalendar(user=request.user, name=title, feed=feed, color=color)
                cal.save()

        return render_to_response('google_calendar/cals.html', {'user': request.user, 'feed_title': feed_title, 'cal_dict': cal_dict})
    #except RequestError as e:
    #Python 2.6 prefers "as", Python 2.5 freaks the fuck out at it
    except RequestError, e:
        return render_to_response('google_calendar/google_error.html', {'status' : e['status'], 'reason': e['reason'], 'body' : e['body']})

@login_required
def next_two_weeks(request):
    """
    Show a list of events for the next two weeks that make the user Busy
    """
    calendar_service = cal_service_for_user(request.user)

    cal_list = GoogleCalendar.objects.filter(user=request.user)

    all_busy = []
    for cal in cal_list:
        query = gdata.calendar.service.CalendarEventQuery(cal.feed, 'private', 'free-busy')
        query.start_min = date.today().isoformat()
        start_max = date.today()+timedelta(14)
        query.start_max = start_max.isoformat()
        query.orderby = 'starttime'
        query.sortorder = 'ascend'
        query.max_results = 50
        feed = calendar_service.CalendarQuery(query)
        busy_times = [dict([('start', parse(w.start_time)),('stop', parse(w.end_time))]) for event in feed.entry for w in event.when]
        all_busy.append({'title':feed.title.text, 'busy_times': busy_times})

    return render_to_response('google_calendar/times.html', {'user': request.user, 'busy': all_busy})

@login_required
@require_GET
def busy_times(request):
    """
    Show busy times for a day for a given calendar
    """

    # try to get the expected keys, but default to None instead of raising
    # KeyError if they're not found
    cal = request.GET.get('cal', None)
    date = request.GET.get('date', None)
    room = request.GET.get('room', '0')

    try:
        Calendar = None
        if room == '1':
            Calendar = RoomGCal.objects.get(room = cal)
        else:
            Calendar = GoogleCalendar.objects.get(id = cal)
        return render_to_response('google_calendar/daybusy.html', { 'busytimes': Calendar.get_busy_times(date)})
    except (GoogleCalendar.DoesNotExist, RoomGCal.DoesNotExist):
        from django.http import HttpResponse
        return HttpResponse("<div id='busytimes'><p style='color: red;'>No calendar exists for this room. Scheduling will fail.</p></div>")

