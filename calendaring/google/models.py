#
# calendaring/google/models.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2009, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.db import models
from django.contrib.auth.models import User, AnonymousUser
from resources.models.other import Room
from resources.models.experiment import ExperimentInstance

import gdata.calendar
import gdata.calendar.service
from gdata.calendar.service import RequestError
import gdata.service
import atom

from datetime import datetime, time
from dateutil.tz import tzlocal
from dateutil.parser import parse

import re

__all__ = ['cal_service_for_user', 'GoogleCredentials', 'GoogleCalendar', 'RoomGCal', 'GoogleEvent',]

# Technically, this ought to be in utils.py, but to use it in this file
# it can't be or it creates a circular dependency
def cal_service_for_user(user):
    creds = None
    try:
        creds = GoogleCredentials.objects.get(user=user)
    except GoogleCredentials.DoesNotExist:
        return None
    calendar_service = gdata.calendar.service.CalendarService()
    calendar_service.SetAuthSubToken(creds.token)
    return calendar_service

class GoogleCredentials(models.Model):
    """
    Credentials for authenticating to a google
    """
    user = models.ForeignKey(User, unique=True, related_name='gcreds')
    token = models.CharField(max_length=64)
    token_valid = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Google Credentials'
        app_label = 'calendaring'

class GoogleCalendar(models.Model):
    """
    A calendar from google
    """
    user = models.ForeignKey(User, related_name='gcals')
    name = models.CharField(max_length=96)
    feed = models.CharField(max_length=256)
    color = models.CharField(max_length=8)

    def user_display_name(self):
        return "%s (%s)" % (self.user.get_full_name(), self.user.email)

    def calendar_url_fragment(self):
        """
        Returns the url parameter fragment for this calendar to build up
        the iframe url, eg. src=user%40gmail.com&amp;color=%23FFFFFF
        """
        from urllib import quote
        calendar_service = cal_service_for_user(self.user)
        
        return "src=%s&amp;color=%s" % (quote(self.feed), quote(self.color))

    def get_busy_times(self, day):
        """
        Checks what times are busy for a given day
        """

        if type(day) == type('') or type(day) == type(u''):
            day = parse(day).date()
        
        calendar_service = cal_service_for_user(self.user)

        query = gdata.calendar.service.CalendarEventQuery(self.feed, 'private', 'free-busy')
        query.start_min = datetime.combine(day, time(0,0,0,0,tzlocal())).isoformat()
        query.start_max = datetime.combine(day, time(23,59,59,0,tzlocal())).isoformat()
        query.orderby = 'starttime'
        query.sortorder = 'ascend'
        query.max_results = 50
        feed = calendar_service.CalendarQuery(query)
        return [dict([('start', parse(w.start_time)),('stop', parse(w.end_time))]) for event in feed.entry for w in event.when]

    def has_free_time(self, start_time, end_time):
        """
        Checks if free during a specified time
        """

        calendar_service = cal_service_for_user(self.user)

        start_time = start_time.replace(tzinfo=tzlocal())
        end_time = end_time.replace(tzinfo=tzlocal())

        query = gdata.calendar.service.CalendarEventQuery(self.feed, 'private', 'free-busy')
        query.start_min = start_time.isoformat()
        query.start_max = end_time.isoformat()
        feed = calendar_service.CalendarQuery(query)

        # any events in the feed indicates that it is a busy time
        if feed.entry:
            return False
        return True

    class Meta:
        app_label = 'calendaring'

class RoomGCal(GoogleCalendar):
    """
    A calendar associated with a room
    """
    room = models.ForeignKey(Room, unique=True)

    class Meta:
        verbose_name = 'Room Google Calendar'
        app_label = 'calendaring'

    def __unicode__(self):
        return u"RoomGCal: " + self.room.__unicode__()

class GEventManager(models.Manager):
    def create(self, subject=AnonymousUser(), title="", exptinst=None, room = None, start_time=None, end_time=None, **kwargs):
        """
        Create an event and add it to the appropriate google calendar. Be sure
        the times include timezone data or google with make it UTC.
    
        subject - django.contrib.auth.models.User object
        title - string of event title
        exptinst - string of the associated ExperimentInstance id
        room - labscheduler.resources.models.Room object
        start_time and end_time - datetime.datetime objects
        """

        roomcal = RoomGCal.objects.get(room=room)

        calendar_service = cal_service_for_user(roomcal.user)

        event = gdata.calendar.CalendarEventEntry()
        event.title = atom.Title(text=title)
        event.content = atom.Content(text='%s -- %s <%s>' % (exptinst.experiment.pub_name, subject.get_full_name(), subject.email))
        event.where.append(gdata.calendar.Where(value_string=room.name))

        event.when.append(gdata.calendar.When(start_time=start_time.isoformat(), end_time=end_time.isoformat()))

        s = gdata.calendar.AttendeeStatus()
        s.value = 'ACCEPTED'
        event.who.append(gdata.calendar.Who(email=subject.email, name=subject.get_full_name(), rel='ATTENDEE', attendee_status=s))
        #TODO: Set it so guests can't invite others or see guest list

        t = gdata.calendar.Transparency()
        t.value = 'OPAQUE' # OPAQUE means busy, TRANSPARENT means available
        event.transparency = t
        
        cal_feed = re.compile(r'^[a-z0-9]+@group.calendar.google.com$')
        gmail_feed = re.compile(r'^[a-zA-Z0-9.-]+@gmail.com$')
        feed_id = None
        if cal_feed.match(roomcal.feed):
            feed_id = '/calendar/feeds/%s/private/full' % roomcal.feed
        elif gmail_feed.match(roomcal.feed):
            feed_id = '/calendar/feeds/default/private/full'
        new_event = calendar_service.InsertEvent(event, feed_id)

        gevent = self.model(subject=subject, room = roomcal,  exptinstance=exptinst, html_link = new_event.GetHtmlLink().href, edit_link = new_event.GetEditLink().href, start_time = start_time, end_time = end_time)
        # using force_insert because models.QuerySet.create(), which is called
        # by models.Manager.create() uses it
        gevent.save(force_insert=True)
        return gevent
    
    class Meta:
        app_label = 'calendaring'

class GoogleEvent(models.Model):
    """
    Link an event on a google calendar with an experiment instance and subject
    """

    subject = models.ForeignKey(User, related_name='gevents')
    room = models.ForeignKey(RoomGCal)
    exptinstance = models.ForeignKey(ExperimentInstance, null=True, blank=True, related_name='when')
    html_link = models.CharField(max_length=256)
    edit_link = models.CharField(max_length=256)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    objects = GEventManager()

    def room_name(self):
        return self.room.room.get_long_name()

    def update_time(self, newstart, newend):
        """
        Update an event with the start and end times given. Be sure the
        times include timezone data or google with make it UTC.
        """

        calendar_service = cal_service_for_user(self.room.user)

        event = calendar_service.GetCalendarEventEntry(self.edit_link)

        # get rid of the old time and add our new start and end time
        event.when.pop()
        start_time = newstart.isoformat()
        end_time = newend.isoformat()
        event.when.append(gdata.calendar.When(start_time=start_time.iso_format(), end_time=end_time.iso_format()))
        calendar_service.UpdateEvent(event.GetEditLink().href, event)

        # updating an event changes the edit link, plus we want to update
        # the start and end times
        self.edit_link = event.GetEditLink()
        self.start_time = newstart
        self.end_time = newend
        self.save()

    def cancel(self):
        """
        Delete the event from google and then delete this event object
        """

        calendar_service = cal_service_for_user(self.room.user)

        try:
            calendar_service.DeleteEvent(self.edit_link)
            self.delete()
        #except RequestError as e:
        #Python 2.6 prefers "as", Python 2.5 freaks the fuck out at "as"
        except RequestError, e:
            from django.shortcuts import render_to_response
            return render_to_response('google_calendar/google_error.html', {'status' : e['status'], 'reason': e['reason'], 'body' : e['body']})

    class Meta:
        app_label = 'calendaring'
