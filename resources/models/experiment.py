#
# resources/models/experiment.py
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from django.db import models
from django.contrib.auth.models import User
from .other import Room, Equipment, Grant, Laboratory, Protocol

__all__ = ['Experiment', 'Requirement', 'ExperimentGroup', 'ExperimentInstance', 'ExperimentCodes',]


class Requirement(models.Model):
    """
    A requirement of a subject participating in an experiment. It works by
    applying the specified function to the subject, which must return True if
    the subject passes and False if they fail.
    """
    name = models.CharField(u"Name", max_length=50, help_text="e.g. Native U.S. English Speaker")
    func_name = models.CharField("Function name", max_length=100, help_text="The name of the function to test with")
    func_args = models.CharField("Function arguments", max_length=100, help_text="Any additonal arguments to pass the function", blank = True)

    def __unicode__(self):
        return self.name

    def check(self, user):
        """
        Takes a string, evaluates it to get a reference to a function, then
        applies it to the provided arguments
        """
        return eval(self.func_name)(user, self.func_args)

    class Meta:
        app_label = 'resources'

class Experiment(models.Model):
    """
    Experiment models an experiment along with all of the required people and
    materials, perquisites, and conflicts.
    """
    pub_name = models.CharField(max_length=30, verbose_name=u"Public Name", help_text = u"Use something descriptive, but that you don't mind subjects seeing", unique=True)
    priv_name = models.CharField(max_length=30, verbose_name=u"Private Name", help_text = u"A potentially more descriptive name for members of the lab")
    desc = models.TextField("Description", help_text="A description of the experiment that subjects can see")
    room = models.ManyToManyField(Room, verbose_name = u"Room")
    equip = models.ManyToManyField(Equipment, null=True, blank=True, verbose_name = u"Required piece of equipment")
    grant = models.ManyToManyField(Grant, limit_choices_to = {'active': True}, verbose_name = u"Grant")
    proto = models.ManyToManyField(Protocol, verbose_name=u"Protocol")
    lab = models.ForeignKey(Laboratory, verbose_name = u"Lab")
    reqperson = models.ManyToManyField(User, limit_choices_to = {'is_staff' : True}, verbose_name = u"Required person or people", null=True, blank=True)
    length = models.IntegerField(u"Length", help_text = u"Give time in minutes")
    days = models.IntegerField(u"Days", help_text = u"How many days does it run?")
    prereq_expts = models.ManyToManyField("ExperimentGroup", verbose_name = u"Prerequisite Experiments", related_name='prereqs', null=True, blank=True)
    conflict_expts = models.ManyToManyField("ExperimentGroup", verbose_name = u"Conflicting Experiments", related_name='conflicts', null=True, blank=True)
    suggested_pairings = models.ManyToManyField("Experiment", null=True, blank=True, help_text="Other experiments that pair well with this one.")
    requirement = models.ManyToManyField(Requirement, null=True, blank=True)
    current = models.BooleanField(u"Currently running?")

    def __unicode__(self):
        return self.priv_name

    # for the sake of Sets all equal comparable objects must have the same hash
    def __hash__(self):
        return hash(self.priv_name)

    def rooms(self):
        """
        Turns the list of room objects into a printable string for display.
        """
        rooms = self.room.all()
        return u', '.join([r.name for r in rooms])

    def equipment(self):
        """
        Turns the list of equipment objects into a printable string for display.
        """
        equips = self.equip.all()
        return u', '.join([e.name for e in equips])

    def grants(self):
        """
        Turns the list of grant objects into a printable string for display.
        """
        grants = self.grant.all()
        return u', '.join([g.name for g in grants])

    def protocols(self):
        """
        Turns the list of proto objects into a printable string for display.
        """
        return u', '.join([p.name for p in self.proto.all()])

    class Meta:
        app_label = 'resources'

class ExperimentGroup(models.Model):
    """
    Allows you to group multiple related experiments together for the purpose
    of making them prerequisites or conflicts of other experiments
    """
    group_name = models.CharField(u"Name", max_length=50, help_text="e.g. John Smith Dissertation Experiments", unique=True)
    experiments = models.ManyToManyField(Experiment, verbose_name = u"Experiments")

    def __unicode__(self):
        return self.group_name

    class Meta:
        app_label = 'resources'

class ExperimentInstance(models.Model):
    """
    A concrete instantiation of an experiment including date/time and subject
    """
    experiment = models.ForeignKey(Experiment, related_name='instances')
    subject = models.ForeignKey(User, related_name='exptinstances')
    assigned_to = models.ForeignKey(User, limit_choices_to = {'is_staff' : True}, related_name='assigned', help_text=u"Staff member responsible for scheduling and/or running the subject in the experiment", blank=True, null=True)
    public_notes = models.TextField(u"Public Notes", default='', blank=True)
    private_notes = models.TextField(u"Private Notes", default='', blank=True)
    completed = models.BooleanField("Completed?", default=False)

    def __unicode__(self):
        return u' '.join((self.subject.__unicode__(), self.experiment.__unicode__()))

    def start_time(self):
        return self.when.get().start_time

    def end_time(self):
        return self.when.get().end_time

    class Meta:
        # we only want one instance of each (experiment, user) pair in the db
        unique_together = (("experiment", "subject"),)
        app_label = 'resources'

class ExperimentCodes(models.Model):
    """
    A set of result codes for experiments.
    """

    subject = models.ForeignKey(User, related_name='excodes')
    experiment = models.ForeignKey(Experiment)
    when = models.DateTimeField("Date and Time")
    ok = models.BooleanField("OK", default=False)
    cancelled = models.BooleanField("Cancelled or Rescheduled Appointment", default=False)
    noshow = models.BooleanField("Did not show up", default=False)
    cheated = models.BooleanField("Cheated (impossibly fast)", default=False)
    understand = models.BooleanField("Did not understand task", default=False)
    computer = models.BooleanField("No computer experience", default=False)
    sleep = models.BooleanField("Extremely tried/falling asleep", default=False)
    interrupt = models.BooleanField("Experiment interrupted (e.g. fire alarm)", default=False)

    def __unicode__(self):
        return self.experiment.__unicode__()

    class Meta:
        app_label = 'resources'
        verbose_name_plural = 'Experiment Codes'
