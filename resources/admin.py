#
# accounts/admin.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from django.contrib import admin
from resources.models import (Experiment, Requirement, ExperimentGroup,
                              ExperimentInstance, ExperimentCodes, Room,
                              Equipment, Grant, Laboratory, Protocol)

class ExperimentCodesInline(admin.StackedInline):
    model = ExperimentCodes
    extra = 1
    max_num = 1
    fieldsets = (
        ('When', {
            'fields' : ('when',)
        }),
        ('Result Codes', {
            'fields' : ('ok', 'cancelled', 'noshow', 'cheated', 'understand', 'computer', 'sleep', 'interrupt',)
        }),
    )

class ExperimentAdmin(admin.ModelAdmin):
    """
    Controls what fields are shown in the django admin interface including
    the order and grouping for presentation.
    """

    fieldsets = (
        (u'Basic Info', {
            'fields': ('pub_name', 'priv_name', 'desc', 'grant', 'proto', 'lab', 'length', 'days', 'room', 'current')
            }),
        (u'Requirements and Restrictions', {
            'fields': ('equip', 'reqperson', 'prereq_expts', 'conflict_expts', 'suggested_pairings')
            }),
    )

    list_display = ('pub_name', 'length', 'days', 'rooms', 'lab', 'grants', 'protocols', 'current')

    list_filter = ('current',)

admin.site.register(Experiment, ExperimentAdmin)

class RequirementAdmin(admin.ModelAdmin):
    pass

admin.site.register(Requirement, RequirementAdmin)

class ExperimentGroupAdmin(admin.ModelAdmin):
    pass

admin.site.register(ExperimentGroup, ExperimentGroupAdmin)

class ExperimentInstanceAdmin(admin.ModelAdmin):
    """
    Controls presentation in the admin interface
    """

    #list_display = ('experiment', 'subject', 'assigned_to', 'public_notes', 'private_notes', 'sched_date', 'start_time', 'end_time', 'completed')
    list_display = ('experiment', 'subject', 'assigned_to', 'public_notes', 'private_notes', 'completed')

    inlines = [ExperimentCodesInline,]

admin.site.register(ExperimentInstance, ExperimentInstanceAdmin)

class LaboratoryAdmin(admin.ModelAdmin):
    """
    Controls what fields are displayed in the django admin interface.
    """
    fieldsets = (
        (None, {
            'fields': ('name', 'pay_per_hour')
            }),
        ('Principals', {
            'fields': ('lab_owner', 'lab_manager', 'lab_staff')
            }),
        ('Contact', {
            'fields': ('lab_room', 'lab_phone')
            }),
    )

    list_display = ('name', 'lab_owner', 'lab_manager', 'lab_room')

admin.site.register(Laboratory, LaboratoryAdmin)

class RoomAdmin(admin.ModelAdmin):
    """
    Controls what fields are displayed in the django admin interface.
    """

    fieldsets = (
        (None, {
            'fields': ('name', 'building', 'number')
            }),
    )

    list_display = ('name', 'building', 'number')

admin.site.register(Room, RoomAdmin)

class EquipmentAdmin(admin.ModelAdmin):
    """
    Controls what fields are displayed in the django admin interface.
    """

    fieldsets = (
        (None, {
            'fields': ('name', 'room')
            }),
    )

    list_display = ('name', 'room')

admin.site.register(Equipment, EquipmentAdmin)

class GrantAdmin(admin.ModelAdmin):
    """
    Controls what fields are displayed in the django admin interface.
    """

    fieldsets = (
        (None, {
            'fields' : ('name', 'principal', 'agency', 'number', 'startdate','enddate','active')}),
    )

    list_display = ('name', 'agency', 'number', 'startdate', 'enddate', 'active')

admin.site.register(Grant, GrantAdmin)

class ProtocolAdmin(admin.ModelAdmin):
    """
    Controls what fields are displayed in the django admin interface.
    """

    fieldsets = (
        (None, {
            'fields' : ('name', 'number', 'principal', 'grant', 'startdate', 'enddate',)}),
    )

    list_display = ('name', 'number', 'startdate', 'enddate')

admin.site.register(Protocol, ProtocolAdmin)
