/*!
 *  Lab Scheduler Staff Scheduling
 *  Author: Andrew Watts
 *  Copyright 2009 - University of Rochester : Brain and Cognitive Sciences
 *  Licensed under the LGPLv2.1: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *  Assumes jquery.js and sched-tools.js are already loaded
*/

$(document).ready(function() {

    // Clear all the inputs and disable all but the subject choice
    disable_and_clear_inputs();

    $(':select[name=subject]').change(function(){
            if ($(this).val() == '') {
                disable_and_clear_inputs();
            } else {
                $.get_expts($(this).val());
            }
        }
    );

    $(':select[name=experiment]').change(function(){
            if ($(this).val() == '') {
                // the change() at the end triggers the cascading reset of the rest of the inputs
                $(':select[name=room]').val('').attr('disabled','disabled').change();
            } else {
                $.get_rooms($(this).val());
            }
        }
    );

    $(':select[name=room]').change(function(){
            var otherinputs = [$(':input[name=date]'),$(':input[name=time]'),$(':input[name=schedule]')];
            if ($(this).val() == '') {
                $.each(otherinputs,function(i,item){item.attr('disabled','disabled');});
                $(':input[name=date]').val('').change();
            } else {
                $.each(otherinputs,function(i,item){item.removeAttr('disabled');});
            }
        }
    );

    $(':input[name=date]').change(function(){
            $('#busytimes').remove();
            if ($(this).val() != '') { $.get_busy_times($(this).val()); }
        }
    );

    }
);

function disable_and_clear_inputs() {
    $(':input[name!=subject]').each(function(i){$(this).attr('disabled','disabled');});
    $(':input[name!=schedule]').each(function(i){$(this).val('');});
}
