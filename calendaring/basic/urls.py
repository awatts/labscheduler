from django.conf.urls.defaults import *

urlpatterns = patterns('labscheduler.basic_calendar.views',
        (r'^$', 'index'),
        (r'^events/$', 'events_for_day'),
)

