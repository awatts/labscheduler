/*!
 *  Lab Scheduler Experiment Scheduling utilities
 *  Author: Andrew Watts
 *  Copyright 2008 - University of Rochester : Brain and Cognitive Sciences
 *  Licensed under the LGPLv2.1: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *  Assumes jquery.js, jquery.tablesorter.js, and sched-tools.js are already loaded
*/

$(document).ready(
    function() {
       $('#available_img').click(function(){load_div('available');});
       $('#scheduled_img').click(function(){load_div('scheduled');});
       $('#completed_img').click(function(){load_div('completed');});

       $(':select[name=room]').live('change', function(){
                if ($(this).val() == '') {
                    $(':select[name=date]').attr('disabled','disabled').val('');
               } else {
                    $(':select[name=date]').removeAttr('disabled');
                }
            }
        );

       $(':select[name=date]').live('change', function() {
                $('#busytimes').remove();
                if ($(this).val() == '') {
                    $(':input[name=time]').attr('disabled','disabled').val('');
                } else {
                    $(':input[name=time]').removeAttr('disabled');
                    $.get_busy_times($(this).val());
                }
            }
        );

       $(':input[name=time]').live('change', function() {
                if ($(this).val() == '') {
                    $(':submit[name=schedule]').attr('disabled','disabled');
                } else {
                    $(':submit[name=schedule]').removeAttr('disabled');
                }
            }
        );

    }
);

function load_div(divname) {
    $('#'+divname+'_img').attr('src', '/media/img/admin/icon_deletelink.gif');
    $('#'+divname+'_img').unbind('click').click(
        function() {
            close_div(divname);
        }
    );

    var data = {};
    $.get("/schedule/experiments/"+divname, data,
        function(respData) {
            $('#'+divname+'_img').after(respData);
            switch(divname) {
                case "available":
                    $('#availtable').tablesorter(
                        {headers: {1:{sorter: false}}}
                    );
                    break;
                case "scheduled":
                    $('#schedtable').tablesorter(
                        {headers: {8:{sorter: false}}}
                    );
                    break;
                case "completed":
                    $('#comptable').tablesorter();
                    break;
            }
            $('#'+divname).fadeIn("slow");
        }
    );
}

function close_div(divname) {
    $('#'+divname+'_img').attr('src', '/media/img/admin/icon_addlink.gif');
    $('#'+divname+'_img').unbind('click').click(
        function() {
            load_div(divname);
        }
    );
    $('#'+divname).slideUp("fast",
        function(){
            $(this).remove();
        }
    );
}

function choose_expt(id) {
    //start by disabling room, which ends up cascading
    $(':select[name=room]').attr('disabled','disabled').val('').change();
    $(':input[name=experiment]').val(id);
    // unhighlight any currently chosen, and highlight the new choice
    $('.chosen').removeClass('chosen').css('background-color','');
    $('#ex' + id  + '> td').addClass('chosen').css('background-color','#FFFF99');
    $.get_rooms(id);
}
