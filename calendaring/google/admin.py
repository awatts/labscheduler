#
# calendaring/google/admin.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from django.contrib import admin
from .models import GoogleCredentials, GoogleCalendar, RoomGCal, GoogleEvent

class GoogleCredentialsAdmin(admin.ModelAdmin):
    list_display = ('user','token','token_valid',)

admin.site.register(GoogleCredentials, GoogleCredentialsAdmin)

class GoogleCalendarAdmin(admin.ModelAdmin):
    list_display = ('user_display_name', 'name', 'color', 'feed',)

admin.site.register(GoogleCalendar, GoogleCalendarAdmin)

class RoomGCalAdmin(admin.ModelAdmin):
    list_display = ('user', 'room', 'name', 'feed',)

admin.site.register(RoomGCal, RoomGCalAdmin)

class GoogleEventAdmin(admin.ModelAdmin):
    list_display = ('subject', 'room_name', 'start_time', 'end_time')

admin.site.register(GoogleEvent, GoogleEventAdmin)
