#
# basic_calendar/admin.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.contrib import admin
from labscheduler.basic_calendar.models import BasicCalendar, Event

class BasicCalendarAdmin(admin.ModelAdmin):
    list_display = ('owner', 'earliest', 'latest', 'weekends',)

admin.site.register(BasicCalendar, BasicCalendarAdmin)

class EventAdmin(admin.ModelAdmin):
    list_display = ('calendar', 'room', 'event_date', 'start_time', 'end_time', 'busy',)

admin.site.register(Event, EventAdmin)
