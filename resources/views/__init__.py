#
# resources/views/__init__.py
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from accounts.models import Survey
from django.shortcuts import render_to_response

#here are other functions in resources/views/ we want to bring in
from .ajax import user_autocomplete, experiment_autocomplete, experiment_room_autocomplete
from .schedule import (schedule_an_experiment, cancel_an_experiment,
                       staff_schedule_subject, mark_completed,
                       create_new_experiment)
from .details import (lab_contacts, experiment_options, available_experiments,
                      completed_experiments, scheduled_experiments, view_subject,
                      view_experiment, view_responsible, view_unmarked_complete)

def index(request):
    """
    The view for the main index page for the scheduler. Gives the user links
    to view their profile or create one, see their list of available
    experiments, and view their messages.

    @return: The rendered html index page
    """

    has_survey = False

    if request.user.is_authenticated():
        # check to see if they've filled out a survey. filter returns a
        # django QuerySet, which will be empty if they haven't filled one out
        survey = Survey.objects.filter(user=request.user.id)
        if survey:
            has_survey = True

    return render_to_response('resources/index.html', {'user': request.user, 'has_survey': has_survey})
