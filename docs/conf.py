extensions = ['sphinx.ext.intersphinx', 'sphinx.ext.todo']

project = 'LabScheduler'

version = '0.9'

release = version + '.0a'

copyright = '2009, University of Rochester - Department of Brain and Cognitive Sciences'

show_authors = True

highlight_language = 'python'

# To use locally generated or cached inv files:
#intersphinx_mapping = {'http://docs.djangoproject.com/en/dev/': 'django.inv', 'http://docs.python.org/': 'python.inv'}
# To use inv files from the projects:
intersphinx_mapping = {'http://docs.djangoproject.com/en/dev/': 'http://docs.djangoproject.com/en/dev/_objects/', 'http://docs.python.org/': ''}

todo_include_todos = True
