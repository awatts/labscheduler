/*!
 *  Lab Scheduler New User functions
 *  Author: Andrew Watts
 *  Copyright 2009 - University of Rochester : Brain and Cognitive Sciences
 *  Licensed under the LGPLv2.1: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *  Assumes jquery.js is already loaded
*/

$(document).ready(function() {
    $("input#id_email").blur(
        function() {
            var data = {};
            $.get('/accounts/check?email=' + $(this).val(), data,
                function(respData) {
                    if(respData) {
                        $('input#id_email').before("<ul id=\"errors\" class=\"errorlist\"></ul>");
                        $('#errors').append(respData);
                    } else {
                        $('#errors').remove();
                    }
                }
            );
        }
    );
});

// Bits of code for the next steps:
// $("tbody").append("<tr><th>Username</th><td id=\"uname\"></td></tr>")
// $("td#uname").append("someusername");
