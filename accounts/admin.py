#
# accounts/admin.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import unicode_literals
from __future__ import absolute_import

from django.contrib import admin
from .models import Survey, Phone, Email, Language, DoNotSchedule

class PhoneInline(admin.StackedInline):
    model = Phone
    extra = 1
    fieldsets = (
        ('Phone Number', {
            'fields': ('phone', 'type',)
        }),
    )

class EmailInline(admin.StackedInline):
    model = Email
    extra = 1
    fieldsets = (
        ('Email Address', {
            'fields': ('email',)
        }),
    )

class LanguageInline(admin.StackedInline):
    model = Language
    extra = 1
    fieldsets = (
        (None, {
            'fields' : ('lang', 'proficient', 'english_country', )
        }),
        ('Dates', {
            'fields': ('started', 'stopped',)
        }),
    )
    radio_fields = {"proficient": admin.HORIZONTAL}


class SurveyAdmin(admin.ModelAdmin):
    """
    Controls what fields are shown in the django admin interface including
    the order and grouping for presentation.
    """

    fieldsets = (
        ('Basic Information', {
            'fields': ('user', 'dob', 'gender')
            }), 
        ('Race', {
            'fields': ('hisp',)
            }),
        ('Ethnicity', {
            'fields': ('amerind', 'afram', 'pacif', 'asian', 'white', 'unk', 'race_other')
            }), 
        ('Student Status', {
            'fields': ('student', 'gradyear', 'gradterm')
            }),
        ('Hearing and Vision', {
            'fields': ('hearing_normal', 'hearing_problems', 'vision_normal', 'vision_other')
            }), 
        ('Studies', {
            'fields': ('more_expts',)
            }),
        )

    list_display = ('user', 'real_name', 'primary_email', 'datefilled' ,'dob', 'gender', 'over_18', 'vision', 'hearing_normal')

    list_filter= ('user', 'gender', 'hearing_normal', 'vision_normal', 'more_expts')

    search_fields = ['user',]

    inlines = [PhoneInline, EmailInline, LanguageInline,]

    radio_fields = {"gender": admin.HORIZONTAL, "vision_normal": admin.HORIZONTAL}

admin.site.register(Survey, SurveyAdmin)

class DoNotScheduleAdmin(admin.ModelAdmin):
    list_display = ('user', 'lab', 'notes')

admin.site.register(DoNotSchedule, DoNotScheduleAdmin)

