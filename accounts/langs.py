# coding=utf-8
#
# langs.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

# list of language options to ensure that the names are consistenly spelled
# the same way

# some possible additions that I've left off for the moment: Croation, Haitian
# Creole, Taiwanese, Fujianese

from __future__ import unicode_literals
from __future__ import print_function

__all__ = ['LANGS',]

LANGS = (
        ('English', 'en - English'),
        ('ASL', 'American Sign Language'),
        ('Spanish', 'es - Español'),
        ('French', 'fr - Français'),
        ('Italian', 'it - Italiano'),
        ('Portuguese', 'pt - Português'),
        ('Romanian', 'ro - Română'),
        ('German', 'de - Deutch'),
        ('Dutch', 'nl - Nederlands'),
        ('Danish', 'da - Dansk'),
        ('Norwegian', 'no - Norsk'),
        ('Swedish', 'sv - Svenska'),
        ('Icelandic', 'is - Íslenska'),
        ('Finnish', 'fi - Suomi'),
        ('Yiddish', 'yi - ייִדיש'),
        ('Hebrew', 'he - עברית'),
        ('Greek', 'el - Ελληνικα'),
        ('Russian', 'ru - Русский'),
        ('Polish', 'pl - Polski'),
        ('Ukrainian', 'uk - Українська'),
        ('Turkish', 'tr - Türkçe'),
        ('Hungarian', 'hu - Magyar'),
        ('Chinese (Mandarin)', 'cmn - 中文 (官話)'),
        ('Chinese (Cantonese)', 'yue - 中文 (粵語)'),
        ('Japanese', 'ja - 日本語'),
        ('Korean', 'ko - 한국어'),
        ('Vietnamese', 'vi - Tiếng Việt'),
        ('Thai', 'th - ภาษาไทย'),
        ('Tagalog', 'tl - Tagalog'),
        ('Hindi', 'hi - हिन्दी'),
        ('Bengali', 'bn - Bengali'),
        ('Gujarati', 'gu - ગુજરાતી'),
        ('Urd', 'ur - اُردو'),
        ('Kannada', 'kn - Kannada'),
        ('Punjabi', 'pa - Punjabi'),
        ('Marathi', 'mr - 	मराठी'),
        ('Malayalam', 'ml - Malayalam'),
        ('Telug', 'te - Telug'),
        ('Tamil', 'ta - தமிழ்'),
        ('Arabic', 'ar - العربية'),
        ('Persian', 'fa - فارسی'),
        ('Pashto', 'pa - پښتو'),
        )

def tupcmp(x,y):
    if x[0] < y[0]:
        return -1
    elif x[0] == y[0]:
        return 0
    elif x[0] > y[0]:
        return 1

if __name__ == '__main__':
    langs = list(LANGS)
    langs.sort(cmp=tupcmp)
    print(langs)
