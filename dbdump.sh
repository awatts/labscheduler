#!/bin/bash

django-admin.py dumpdata --settings="labscheduler.settings" --pythonpath="$HOME/django_apps/" --format=xml --indent=4 resources accounts calendar > dbcontents.xml
