#
# djangoCalAuth.py
#
# Author: Andrew Watts
# Created: Tue 03-Jun-2008 10:49 EDT
#
# Copyright (c) 2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

"""
Django User/Group compatible directory service for Apple's CalendarServer

Modeled on the xmlfile.py file that comes with CalendarServer
"""

__all__ = [
        "DjangoDirectoryService",
        ]

from twistedcaldav.directory.directory import DirectoryService, DirectoryRecord
from twistedcaldav.directory.directory import DirectoryError, UnknownRecordTypeError

from django.contrib.auth.models import User, Group
from labscheduler.resources.models import Room, Equipment

from labscheduler.darwin_calendar.models import *

class DjangoDirectoryService(DirectoryService):
    """
    django.contrib.auth based implementation of IDirectoryService
    """
    baseGUID = "3D483003-4289-507E-8637-A11BBB12658F"
    
    realmName = None

    def __init__(self, realmName):
        super(DjangoDirectoryService, self).__init__()
        realmName = realmName

    def __repr__(self):
        return "<%s %r>" % (self.__class__.__name__, self.realmName)

    def recordTypes(self):
        recordTypes = (
            DirectoryService.recordType_users,
            DirectoryService.recordType_groups,
            DirectoryService.recordType_locations,
            DirectoryService.recordType_resources,
        )
        return recordTypes

    def listRecords(self,recordType):
        pass

    def recordWithShortName(self,recordType, shortName):
        pass

    def _entriesForRecordType(self, recordType):
        typemap = {
            DirectoryService.recordType_users : User,
            DirectoryService.recordType_groups : Group,
            DirectoryService.recordType_locations : Room,
            DirectoryService.recordType_resources : Equipment,
        }

        for entry in typemap[recordType].objects.all():
            yield entry.

class DjangoDirectoryRecord(DirectoryRecord):
    """
    django.contrib.auth based implementation of IDirectoryRecord
    """
    def __init__(self, service, recordType, shortName, djangoPrincipal):
        super(DjangoDirectoryRecord, self).__init__(
            service               = service,
            recordType            = recordType,
            guid                  = djangoPrincipal.guid,
            shortName             = shortName,
            fullName              = djangoPrincipal.name,
            calendarUserAddresses = djangoPrincipal.calendarUserAddresses,
            autoSchedule          = djangoPrincipal.autoSchedule,
            enabledForCalendaring = djangoPrincipal.enabledForCalendaring      
        )

        self.password = djangoPrincipal.password

        def members(self):
            pass

        def groups(self):
            pass

        def proxies(self):
            pass

        def proxyFor(self, read_write=True):
            pass

        def readOnlyProxies(self):
            pass

        def readOnlyProxyFor(self, read_write=True):
            pass

        def verifyCredentials(self, credentials):
            pass
