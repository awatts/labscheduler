.. _reports-views:

==========================================
LabScheduler report views
==========================================

.. module:: labscheduler.reports.views
    :synopsis: Views for reports about enrollment, etc.

Overview
========

The point of reports is to generate reports about information that IRBs or grant agencies would want to know. All reports are meant only to be views by staff, and are wrapped in the following dectorator

.. code-block:: python

   @user_passes_test(lambda u: u.is_staff)

built on the :func:`django.contrib.auth.decorators.user_passes_test` decorator, and checking that the user is staff.

Views
=====

language_summary
----------------

.. function:: language_summary(request)

    Generates a summary of how many people in the subject pool speak each language

new_enrollment
--------------

.. function:: new_enrollment(request)

    Gets the count and names of subjects who have enrolled this calendar year

    .. todo::

      Possible future change: take a year as a parameter and get the subjects who enrolled that year

    .. warning::

      Unimplemented function

missed_experiments
------------------

.. function:: missed_experiments(request)

    Finds all experiment appointments that have passed without having been marked as completed, giving the option to mark or reschedule. 
    
    .. warning::
        
        Unimplemented function

willing_subjects
----------------

.. function:: willing_subjects(request, type='html')

    :param type: A string (either 'html' or 'text') of how to generate the list

    Generates a list of subjects willing to participate in future experiments. Can either generated a web page with the list or a downloadable text file.

dns_list
--------

.. function:: dns_list(request)

    Generates a list of subjects who are not to be scheduled for any future experiments in the staff member's lab (i.e. have :class:`~labscheduler.accounts.models.DoNotSchedule` objects), where the staff member is the value of the :attr:`~django.http.HttpRequest.user` in *request*.

expt_codes_and_count
--------------------

.. function:: expt_codes_and_count(request)

    Generates a list of experiment instances for all labs that the :attr:`~django.http.HttpRequest.user` in *request* is lab staff in including the :class:`~labscheduler.resources.models.ExperimentCodes` and count of times the experiment has been done.
