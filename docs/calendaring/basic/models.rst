.. _calendaring_basic-models:

=====================
Basic Calendar Models
=====================

.. module:: labscheduler.calendaring.basic.models
    :synopsis: Models related to the basic, dumb calendar

Overview
========
The following models are provided:

 * :class:`BasicCalendar`
 * :class:`Event`

BasicCalendar
=============

.. class:: BasicCalendar

API Reference
-------------

Fields
~~~~~~

.. attribute:: BasicCalendar.owner

    A :class:`~django.contrib.auth.models.User` object for the owner of the calendar.

.. attribute:: BasicCalendar.earliest

    A :class:`datetime.time` object for the earliest time to schedule subjects

.. attribute:: BasicCalendar.latest
    
    A :class:`datetime.time` object for the latest time to schedule subjects

.. attribute:: BasicCalendar.weekends

    A boolean of whether to schedule subjects on weekends. Default :data:`False`.

Methods
~~~~~~~

.. method:: __unicode__

    Returns the *__unicode__* string form of the :attr:`~BasicCalendar.owner`

Event
=====

.. class:: Event

API Reference
-------------

Fields
~~~~~~

.. attribute:: Event.calendar

    The :class:`BasicCalendar` calendar that this event belongs to.

.. attribute:: Event.room

    The :class:`~labscheduler.resources.models.Room` where the event takes place.

.. attribute:: Event.invitees

    A :class:`~django.db.models.ManyToManyField` of :class:`~django.contrib.auth.models.User` who should be at the event.

.. attribute:: Event.event_date

    The date of the event.

.. attribute:: Event.start_time

    The start time of the event.

.. attribute:: Event.end_time

    The end time of the event.

.. attribute:: Event.busy

    Boolean of whether this event is "Busy" and blocks overlapping events.

.. attribute:: Event.purpose

    String of what this event is.

Methods
~~~~~~~

.. method:: __unicode__

    Returns a string like "YYYY-MM-DD@HH:MM" with :attr:`start_date` and :attr:`start_time`
