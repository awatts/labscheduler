#
# calendaring/basic/models.py
#
# Author: Andrew Watts
# Created: Mon 16-Jun-2008 14:17 EDT
#
# Copyright (c) 2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.db import models
from django.contrib.auth.models import User, Group
from labscheduler.scheduler.models import Room, Equipment

class BasicPrincipal(models.Model):
    """
    Abstract Base Class for calendaring principals
    """

    proxies = models.ManyToManyField(BasicPrincipal, related_name="%(class)s_related")

    class Meta:
        abstract = True

class UserPrincipal(BasicPrincipal):
    """
    A django user who can own a calendar
    """

    django_user = models.ForeignKey(User, related_name="%(class)s_related", unique=True)

class GroupPrincipal(BasicPrincipal):
    """
    A group (TBD: a django group or some sort of internal group) which can
    own a calendar
    """
    pass

class RoomPrincipal(BasicPrincipal):
    """A room that can be scheduled"""

    room = models.ForeignKey(Room, related_name="%(class)s_related", unique = True)

class EquipmentPrincipal(BasicPrincipal):
    """Equipment that can be scheduled"""

    equipment = models.ForeignKey(Equipment, related_name="%(class)s_related", unique = True)

class BasicCalendar(models.Model):
    """
    A simple, no-frills calendar to use when you don't want to run your own
    calendar server or connect to an outside calendar server.
    """
    
    owner = models.ForeignKey(BasicPrincipal, unique=True)

class Appointment(models.Model):
    """
    A timeslot when an event occurs.
    """

    initiator = models.ForeignKey(BasicPrincipal)
    invitees = models.ManyToManyField(BasicPrincipal)

    calendar = models.ForeignKey(BasicCalendar, related_name="%(class)s_related")

    start_time = models.DateTimeField() 
    end_time = models.DateTimeField()

    busy = models.BooleanField(default = True, help_text = u'Mark as busy? i.e. Disallow other overlapping events.')
