.. _calendaring_basic-views:

====================
Basic Calendar Views
====================

.. module:: labscheduler.calendaring.basic.views
    :synopsis: Views related to the basic calendar

Views
=====

index
-----
.. function:: index(request)

    Default index page showing the user any events they are involved in for the next week

    .. warning::

        If the user doesn't have a :class:`~labscheduler.calendaring.basic.models.BasicCalendar` it creates one defaulting to 9am :attr:`~labscheduler.calendaring.basic.models.BasicCalendar.earliest` and 5pm :attr:`~labscheduler.calendaring.basic.models.BasicCalendar.latest`


list_freetimes
--------------
.. function:: list_freetimes(request)

    Should return a list of free times

    .. warning::

        Not implemented. Returns a :class:`django.http.HttpResponseBadRequest`.

events_for_day
--------------
.. function:: events_for_day(request)

    Gives a staff member a list of all :class:`~labscheduler.calendaring.basic.models.Event` scheduled for a given day.
