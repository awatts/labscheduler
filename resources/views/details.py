#
# resources/views/details.py
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponseBadRequest
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required, user_passes_test
from accounts.models import Survey
from ..models import Laboratory, Experiment, ExperimentInstance
from calendaring.models import RoomCal
from ..utils import get_available
from ..forms import ScheduleExptForm

from datetime import date, timedelta
from dateutil.parser import parse

__all__ = ('lab_contacts', 'experiment_options', 'available_experiments', 'completed_experiments', 'scheduled_experiments', 'view_subject', 'view_experiment', 'view_responsible', 'view_unmarked_complete',)

def lab_contacts(request):
    """
    Show contact information for all labs using this system
    """
    labs = Laboratory.objects.all()
    return render_to_response('resources/labcontacts.html', {'user' : request.user, 'labs': labs})

@login_required
def experiment_options(request):
    """
    Show the list of experiment options
    """

    exptinstances = request.user.exptinstances.all()

    available = len(get_available(request.user))
    completed = exptinstances.filter(completed = True).count()
    scheduled = exptinstances.filter(completed = False).count()

    return render_to_response('resources/experiments.html', {'user' : request.user, 'available' : available, 'completed' : completed, 'scheduled': scheduled})

@login_required
def available_experiments(request):
    """
    Show the user the experiments they are eligible to participate in.
    """

    available = get_available(request.user)
    form = ScheduleExptForm()

    return render_to_response('resources/available.html', {'user' : request.user, 'available' : available, 'form': form})

@login_required
def completed_experiments(request):
    """
    Show the user the experiments they have completed.
    """
    completed = request.user.exptinstances.filter(completed = True)

    return render_to_response('resources/completed.html', {'user' : request.user, 'completed' : completed})

@login_required
def scheduled_experiments(request):
    """
    Show the user the experiments they have scheduled.
    """
    scheduled = request.user.exptinstances.filter(completed = False)

    return render_to_response('resources/scheduled.html', {'user' : request.user, 'scheduled' : scheduled})

@user_passes_test(lambda u: u.is_staff)
def view_subject(request, uname=None):
    """
    Show minimal info about a subject to aid in scheduling them.
    """

    djuser = get_object_or_404(User, username = uname)
    subject = get_object_or_404(Survey, user = djuser)
    emails = subject.emails.all()
    phones = subject.phones.all()
    languages = subject.languages.all()
    completed = djuser.exptinstances.filter(completed = True)

    return render_to_response('resources/staff/subjectview.html', {'user' : request.user, 'subject' : subject, 'emails' : emails, 'phones' : phones, 'languages': languages, 'completed': completed})

@login_required
def view_experiment(request, id, day=date.today().isoformat()):
    """
    Show full information about an experiment.
    """

    if request.REQUEST.has_key('day'):
        day = request.REQUEST['day']

    day = parse(day).date()

    if day < date.today():
        return HttpResponseBadRequest("Cannot chose dates before today.")

    if day > date.today() + timedelta(14):
        return HttpResponseBadRequest("Cannot chose dates more than two weeks in the future.");

    experiment = get_object_or_404(Experiment, id=id)

    schedule = False
    if experiment in get_available(request.user):
        schedule = True

        roomtimes = []
        for room in experiment.room.all():
            try:
                roomcal = RoomCal.objects.get(room=room)
                roomtimes.append(dict([('title', room.get_long_name()), ('busy_times', roomcal.get_busy_times(day))]))
            except RoomCal.DoesNotExist:
                pass # just skip this room if it has no calendar

        weekend = False
        if day.weekday() in [5,6]:
            weekend = True

        # only create a yesterday link for days after today
        yesterday = None
        if day > date.today():
            yesterday = day + timedelta(-1)
            yesterday = yesterday.isoformat()

        # only create tomorrow links for the next two weeks
        tomorrow = None
        if day < date.today() + timedelta(14):
            tomorrow = day + timedelta(1)
            tomorrow = tomorrow.isoformat()

    return render_to_response('resources/exptview.html', {'user' : request.user, 'experiment' : experiment, 'schedule': schedule, 'day': day, 'yesterday': yesterday, 'tomorrow': tomorrow, 'roomtimes': roomtimes, 'weekend': weekend})

@login_required
def view_responsible(request):
    """
    Show all experiment instances the user is responisble for
    """

    responsible = ExperimentInstance.objects.filter(assigned_to = request.user)

    return render_to_response('resources/responsible.html', {'user': request.user, 'resp': responsible})

#@user_passes_test(lambda u: u.is_staff)
def view_unmarked_complete(request):
    """
    Show all experiments that should have been completed, but haven't been
    marked as such yet.
    """

    # start with all uncompleted expriments in a lab where the user is staff
    uncompleted = ExperimentInstance.objects.filter(completed = False, experiment__lab__in = request.user.labstaff.all())

    return render_to_response('resources/uncompleted.html', {'user': request.user, 'uncomplete': uncompleted})
