#
# basic_calendar/controllers.py
#
# Author: Andrew Watts
#
# Copyright (c) 2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from datetime import time, datetime
from dateutil.tz import tzlocal
from labscheduler.basic_calendar.models import Event

def gen_time_slots(room, date, start, end):
    """
    Given a date (datetime.date object), and start and end hours (integers),
    returns a list of open timeslots.
    """

    # no time slots on weekends
    if date.weekday() in (5,6):
        return []

    #slots = [time(t,0) for t in range(start,end)] + [time(t,30) for t in range(start,end)]
    slots = [datetime.combine(date, time(t,0,0,0,tzlocal())) for t in range(start,end)]
    #slots.sort()
    
    for e in Event.objects.filter(event_date = date, room=room):
        for s in slots:
            if e.start_time <= s < e.end_time:
                slots.remove(s)
    
    return slots

def check_time_room(room, date, start):
    """
    Check that a room is free for a time
    """

    for e in Event.objects.filter(event_date = date, room=room):
        if e.start_time <= start < e.end_time:
            return False
    return True
