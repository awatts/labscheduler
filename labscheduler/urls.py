from os import environ
from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'labscheduler.views.home', name='home'),
    # url(r'^labscheduler/', include('labscheduler.foo.urls')),
    (r'^$', 'resources.views.index'),
    (r'^labcontacts/$', 'resources.views.lab_contacts'),
    (r'^schedule/', include('resources.urls')),
    (r'^accounts/', include('accounts.urls')),
    (r'^reports/', include('reports.urls')),
    (r'^calendar/', include('calendaring.urls')),
    # this is just for devel. for production, the webserver should serve styles
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': environ.get('HOME') + '/django_apps/labscheduler/media'}),

    # Uncomment the admin/doc line below to enable admin documentation:
    (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
