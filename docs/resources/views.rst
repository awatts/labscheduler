.. _resources-views:

==========================================
LabScheduler schedulable resources views
==========================================

.. module:: labscheduler.resources.views
    :synopsis: Views related to schedulable resources

Overview
========

The views for resources can be divided into three groups: AJAX, Scheduling, and Details

.. function:: index

AJAX
----

.. function:: user_autocomplete

.. function:: experiment_autocomplete

.. function:: experiment_room_autocomplete

Scheduling
----------

.. function:: schedule_an_experiment

.. function:: cancel_an_experiment

.. function:: staff_schedule_subject

.. function:: mark_completed

Details
-------

.. function:: lab_contacts

.. function:: experiment_options

.. function:: available_experiments

.. function:: completed_experiments

.. function:: scheduled_experiments

.. function:: view_subject

.. function:: view_experiment

.. function:: view_responsible

.. function:: view_unmarked_complete
