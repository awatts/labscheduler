#
# resources/views/ajax.py
#
# Copyright (c) 2007-2008, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.views.decorators.http import require_GET

from ..models import Experiment
from json_response import JsonResponse
from ..utils import get_available

__all__ = ('user_autocomplete', 'experiment_autocomplete', 'experiment_room_autocomplete',)

@user_passes_test(lambda u: u.is_staff)
@require_GET
def user_autocomplete(request):
    """
    Based on partial name, suggest possible users. Only return the first 10 to
    avoid swamping the DB or user.

    This function is not used anymore, but is probably worth keeping just in case
    """

    #if not request._is_ajax():
    #    return HttpResponseBadRequest('This is an AJAX only view')

    name = request.GET.get('subject').split(' ')
    suggest = None

    if len(name) == 1:
        suggest = User.objects.filter(first_name__startswith=name[0])[:10]
    if len(name) == 2:
        suggest = User.objects.filter(first_name__iexact = name[0]).filter(last_name__startswith=name[1])[:10]

    return render_to_response('resources/namecomplete.html', {'suggest': suggest})

#@user_passes_test(lambda u: u.is_staff)
@login_required
@require_GET
def experiment_autocomplete(request):
    """
    Based on a user id, get list of available experiments
    """
    
    #if not request._is_ajax():
    #    return HttpResponseBadRequest('This is an AJAX only view')
    
    uid = request.GET.get('uid')
    
    if request.user.id != int(uid):
        if not request.user.is_staff:
            return HttpResponseBadRequest('Only staff or the user in question can request this page.') 

    subject = None
    try:
        subject = User.objects.get(id = uid)
    except User.DoesNotExist:
        return JsonResponse(None)

    expts = [dict(zip(('id','pub_name', 'lab_name'),(e.id, e.pub_name, e.lab.name))) for e in get_available(subject)]
    expts = dict(zip(["ex" + str(x['id']) for x in expts], expts))
    return JsonResponse(expts)

@user_passes_test(lambda u: u.is_staff)
@require_GET
def experiment_room_autocomplete(request):
    """
    Based on an experiment id, get a list of rooms it can be run in
    """
    
    #if not request._is_ajax():
    #    return HttpResponseBadRequest('This is an AJAX only view')

    eid = request.GET.get('eid')
    expt = None
    try:
        expt = Experiment.objects.get(id = eid)
    except Experiment.DoesNotExist:
        return JsonResponse(None) 
    
    rooms = [dict(zip(('id','name'),(r.id, r.name + ' (' + r.building + ' ' + r.number + ')'))) for r in expt.room.all()]
    rooms = dict(zip(["r" + str(x['id']) for x in rooms], rooms))
    return JsonResponse(rooms)
