/*!
 *  Lab Scheduler Scheduling utilities
 *  Author: Andrew Watts
 *  Copyright 2008-2009 - University of Rochester : Brain and Cognitive Sciences
 *  Licensed under the LGPLv2.1: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *  Assumes jquery.js is already loaded
*/


(function($) {

$.get_expts = function(uid) {
    $(':select[name=experiment]').children(':not(:first-child)').remove();
    var data = {};
    $.getJSON('/schedule/staff/exptcomplete/?uid=' + uid, data,
        function(respData, textStatus) {
            switch(textStatus) {
                case 'success':
                    $.each(respData, function(i,item) {
                        $("<option/>").attr("value", item.id).html(item.lab_name + ' : ' + item.pub_name).appendTo(':select[name=experiment]');
                        }
                    );
                    $(':select[name=experiment]').removeAttr('disabled');
                    break;
                case 'error':
                    $(':select[name=experiment]').after("<p class='error'>Error retrieving experiment list.</p>");
                    break;
                case 'timeout':
                    $(':select[name=experiment]').after("<p class='error'>Error: Connection to server timed out.</p>");
                    break;
                case 'parsererror':
                    $(':select[name=experiment]').after("<p class='error'>Error: Malformed data recieved.</p>");
                    break;
                case 'notmodified':
                    // shouldn't ever get this
                    break;
            }
        }
    );
}

$.get_rooms = function(eid) {
    $(':select[name=room]').children(':not(:first-child)').remove();
    var data = {};
    $.getJSON('/schedule/staff/roomcomplete/?eid=' + eid, data,
        function(respData, textStatus) {
            switch(textStatus) {
                case 'success':
                    $.each(respData, function(i,item){
                        $("<option/>").attr("value", item.id).html(item.name).appendTo(':select[name=room]');
                        }
                    );
                    $(':select[name=room]').removeAttr('disabled');
                    break;
                case 'error':
                    $(':select[name=experiment]').after("<p class='error'>Error retrieving room list.</p>");
                    break;
                case 'timeout':
                    $(':select[name=experiment]').after("<p class='error'>Error: Connection to server timed out.</p>");
                    break;
                case 'parsererror':
                    $(':select[name=experiment]').after("<p class='error'>Error: Malformed data recieved.</p>");
                    break;
                case 'notmodified':
                    // shouldn't ever get this
                    break;
            }
        }
    );
}

$.get_busy_times = function(day) {
    $.ajax({
        url: '/calendar/busytimes/?room=1&cal=' + $(':select[name=room]').val() + '&date=' + day,
        beforeSend: function() {
            $(':select[name=date]').after("<p class='loading'>Loading...</p>");
        },
        success: function(respData, textStatus) {
            $('p.loading').remove();
            $(':select[name=date]').after(respData);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            $('p.loading').remove();
            switch(textStatus) {
                case 'error':
                    $(':select[name=date]').after("<p class='error'>Error fetching busy times.</p>");
                    break;
                case 'timeout':
                    $(':select[name=date]').after("<p class='error'>Error: Connection to server timed out.</p>");
                    break;
                case 'parsererror':
                    $(':select[name=date]').after("<p class='error'>Error: Malformed data recieved.</p>");
                    break;
                default:
                    $(':select[name=date]').after("<p class='error'>An undefined error has occured.</p>");
                    break;
            }
        },
        dataType: 'html'
    });
}

})(jQuery);
