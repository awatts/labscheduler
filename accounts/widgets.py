#
# widgets.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import
from __future__ import unicode_literals

from django import forms
from django.utils.encoding import force_unicode

__all__ = ('HispanicSelect', 'RaceMultiWidget', 'BooleanSelect',
           'RadioSideBySideRenderer', 'BooleanRadioSelect',)

class HispanicSelect(forms.Select):
    """
    A widget subclassed from django.forms.widgets.Select, intended for use
    with a NullBooleanField with the options "Hispanic or Latino", "Not Hispanic
    or Latino", and "Decline to answer"

    Most of the code for this function is copied from django's NullBooleanSelect
    which I would have subclassed, but they won't let you override choices.
    """
    def __init__(self, attrs=None):
        choices = (('1', 'Decline to Answer'), ('2', 'Hispanic or Latino'), ('3', 'Not Hispanic or Latino'))
        super(HispanicSelect, self).__init__(attrs, choices)

    def render(self, name, value, attrs=None, choices=()):
        try:
            value = {True: '2', False: '3', '2': '2', '3': '3'}[value]
        except KeyError:
            value = '1'
        return super(HispanicSelect, self).render(name, value, attrs, choices)

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        return {'2': True, '3': False, True: True, False: False}.get(value, None)

class RaceMultiWidget(forms.MultiWidget):
    """
    A widget subclassed from django.forms.widgets.MultiWidget for displaying
    all of the race options in one widget with one return value. See the django
    docs for more info about the methods being subclassed.
    """
    #
    # This should not be used. It's not quite right anymore.
    #
    def __init__(self, attrs=None):
        widgets = (
            forms.CheckboxSelectMultiple(choices=(
                ('1', "American Indian / Alaska Native"),
                ('2', "Black or African-American"),
                ('3', "Native Hawaiian or Other Pacific Islander"),
                ('4', "Asian"),
                ('5', "White"),
                ('6', "Unknown"),
                )
            ),
            )
        super(RaceMultiWidget, self).__init__(widgets, attrs)

    def decompress(self, value):
        if value:
            rv = []
            for v in value:
                if v == 'checked':
                    rv.append(True)
                else:
                    rv.append(False)
            return rv
        return([False, False, False, False, False, False])

    def format_output(self, rendered_widgets):
        return rendered_widgets[0] + "Other: %s" % rendered_widgets[1]

class BooleanSelect(forms.Select):
    """
    A widget subclassed from django.forms.widgets.Select to do a simple
    Boolean choice in the form of an html select widget.
    """
    def __init__(self, *args, **kwargs):
        choices = ((True, 'Yes'), (False, 'No'),)
        super(BooleanSelect, self).__init__(choices=choices, *args, **kwargs)

class RadioSideBySideRenderer(forms.widgets.RadioFieldRenderer):
    """
    Overrides the default renderer which makes an unordered html list. Instead
    this renders the buttons side by side.
    """
    def render(self):
        return ' '.join([force_unicode(w) for w in self])

class BooleanRadioSelect(forms.RadioSelect):
    """
    A widget subclassed from django.forms.widgets.BooleanSelect to do a
    simple Boolean choice in the form of a two button html radio widget.
    """
    def __init__(self, *args, **kwargs):
        choices = (('1', 'Yes'), ('2', 'No'),)
        super(BooleanRadioSelect, self).__init__(choices=choices, renderer=RadioSideBySideRenderer, *args, **kwargs)

    def render(self, name, value, attrs=None, choices=()):
        value = {True: '1', False: '2', None: ''}[value]
        return super(BooleanRadioSelect, self).render(name, value, attrs, choices)

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        return {'1': True, '2': False}.get(value)

class CalendarWidget(forms.TextInput):
    class Media:
        css = {
            #'all': ['/site_media/css/theme/' + c for c in ('ui.core.css', 'ui.theme.css', 'ui.datepicker.css',)]
            'all': ('http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/themes/blitzer/jquery-ui.css',)
        }

        js = ('http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js', '/site_media/js/survey-tools.js',)
