from django import template
from django.template.defaultfilters import stringfilter
from hashlib import sha1 as sha
import datetime

register = template.Library()

@register.filter
@stringfilter
def obfuscate(value, fill="NOSPAM"):
    """Obfuscates an email address to reduce harvesting"""
    return value.replace('@', ' AT ' + fill.upper() + ' ').replace('.', ' DOT ')

@register.filter
@stringfilter
def shahash(value, salt="foo"):
    """Uses SHA to hash the value with today's date"""
    return sha(str(value).strip() + str(salt) + datetime.date.today().isoformat()).hexdigest()
