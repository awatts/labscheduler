#
# choices.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import unicode_literals

__all__ = ['GENDER_CHOICES', 'TERM_CHOICES', 'VISION_CHOICES', 'PHONE_CHOICES', 'PROFICIENTY_CHOICES',]

GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
        ('N', 'Not specified'),
        )

TERM_CHOICES = (
        ('SP', 'Spring'),
        ('AU', 'Autumn'),
        ('SU', 'Summer'),
        )

VISION_CHOICES = (
        (0, 'Normal uncorrected'),
        (1, 'Corrected-to-normal with glasses'),
        (2, 'Corrected-to-normal with soft contacts'),
        (3, 'Corrected-to-normal with hard contacts'),
        (4, 'Other'),
        )

PHONE_CHOICES = (
        ('home', 'Home'),
        ('work', 'Work'),
        ('cell', 'Cell'),
        )

PROFICIENTY_CHOICES = (
        (4, 'Native'),
        (3, 'Near-native fluency'),
        (2, 'Competent'),
        (1, 'Novice'),
        )
