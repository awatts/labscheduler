.. _calendaring_darwin-index:

:mod:`~labschedler.calendaring.darwin` -- Connecting to Apple's Darwin Calendar Server
======================================================================================

.. module:: labscheduler.calendaring.darwin
   :synopsis: A calendaring backend backend for Apple's Darwin Calendar Server
.. moduleauthor:: Andrew Watts <awatts@bcs.rochester.edu>

.. toctree::
   :maxdepth: 1
   
   models
   views
