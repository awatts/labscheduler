#
# resources/models/other.py
#
# Copyright (c) 2007-2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.db import models
from django.contrib.auth.models import User
from django.contrib.localflavor.us.models import PhoneNumberField

__all__ = ['Laboratory', 'Room', 'Equipment', 'Grant', 'Protocol',]

class Laboratory(models.Model):
    """
    Laboratory is a model of some basic info about a lab.
    This is basically just here so you can have multiple labs running on
    the same instance with a common subject pool.
    """
    name = models.CharField(max_length=30)
    lab_owner = models.ForeignKey(User, related_name='ownlab', limit_choices_to = {'is_staff' : True}, verbose_name= u'Lab Owner')
    lab_manager = models.ForeignKey(User, related_name='managedlab', limit_choices_to = {'is_staff' : True}, verbose_name= u'Lab Manager')
    lab_staff = models.ManyToManyField(User, related_name='labstaff', limit_choices_to = {'is_staff' : True}, verbose_name = u'Lab Staff')
    lab_room = models.ForeignKey("Room")
    lab_phone = PhoneNumberField(u"Phone number")
    pay_per_hour = models.DecimalField(u"Pay per hour", help_text = u"What subjects are paid per hour for experiments (in dollars).", decimal_places=2, max_digits=4)

    def __unicode__(self):
        return self.name

    def lab_owner_phone(self):
        try:
            return self.lab_owner.surveys.all()[0].phones.filter(type='work')[0].phone
        except IndexError:
            return 'Unlisted'

    def lab_manager_phone(self):
        try:
            return self.lab_manager.surveys.all()[0].phones.filter(type='work')[0].phone
        except IndexError:
            return 'Unlisted'

    class Meta:
        """
        Controls some metadata for the class. See django docs for more detail.
        """
        verbose_name= u"laboratory"
        verbose_name_plural= u"laboratories"
        app_label = 'resources'

class Room(models.Model):
    """
    Room models an actual physical location of a lab. It allows us to schedule
    the same experiment in several places at once as well as allowing rooms
    to be reserved for other purposes that would block experiments.
    """
    name = models.CharField(max_length=50) # e.g. Eyelink Room, Coding Room
    building = models.CharField(max_length=30)
    number = models.CharField(max_length=30)

    def __unicode__(self):
        return self.name

    def get_long_name(self):
        return u"%s %s" % (self.building, self.number)

    class Meta:
        app_label = 'resources'

class Equipment(models.Model):
    """
    Equipment models a piece of equipment necessary for an experiment.
    """
    name = models.CharField(max_length=30)
    room = models.ForeignKey(Room, related_name='equipment', verbose_name = u'Room', help_text = u"Room where it is normally kept")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = u"equipment"
        verbose_name_plural = u"equipment"
        app_label = 'resources'

class Grant(models.Model):
    """
    Grant models a grant that funds experiments.
    """
    name = models.CharField(u"Name", max_length=75, help_text = u"e.g. 'Sentence processing'", unique=True)
    principal = models.ManyToManyField(User, limit_choices_to = {'is_staff' : True}, verbose_name= u'Principal Investigators')
    agency = models.CharField(u"Funding agency", max_length=30)
    number = models.CharField(u"Grant number", max_length=30)
    startdate = models.DateField(u"Start Date")
    enddate = models.DateField(u"End Date")
    active = models.BooleanField(u"Active?", default = False)

    def __unicode__(self):
        return u"%s %s" % (self.agency, self.number)

    class Meta:
        app_label = 'resources'

class Protocol(models.Model):
    """
    The IRB/RSRB/whatever protocol an experiment runs under
    """
    name = models.CharField(u"Name", max_length=75, help_text = u"e.g. 'Sentence processing'", unique=True)
    principal = models.ManyToManyField(User, limit_choices_to = {'is_staff' : True}, verbose_name= u'Principal Investigators')
    grant = models.ManyToManyField(Grant)
    number = models.CharField(u"IRB number", max_length=30)
    startdate = models.DateField(u"Start Date")
    enddate = models.DateField(u"End Date")

    def __unicode__(self):
        return u"Protocol %s %s" % (self.name, self.number) 
    
    class Meta:
        app_label = 'resources'
