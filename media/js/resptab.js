/*!
 *  Author: Andrew Watts
 *  Copyright 2008 - University of Rochester : Brain and Cognitive Sciences
 *  Licensed under the LGPLv2.1: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html
 *  Assumes jquery.js and jqeuery.tablesorter.js are already loaded
*/

$(document).ready(function() { 
        $(".tablesorter").tablesorter({headers: { 7: { sorter: false}, 8: {sorter: false} }}); 
        //$(".tablesorter").tablesorter(); 
    } 
);
