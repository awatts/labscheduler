from django.conf.urls.defaults import *

urlpatterns = patterns('resources.views',
        #(r'^$', 'index'),
        (r'experiments/$', 'experiment_options'),
        (r'experiments/available/$', 'available_experiments'),
        #(r'experiments/available/(?P<id>\d+)/$', 'view_experiment'),
        #(r'experiments/available/(?P<id>\d+)/(?P<day>\d{4}-\d{2}-\d{2})/$', 'view_experiment'),
        (r'experiments/scheduled/$', 'scheduled_experiments'),
        (r'experiments/completed/$', 'completed_experiments'),
        #(r'experiments/doschedule/(?P<eid>\d+)/(?P<rid>\d+)/(?P<edate>\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2})/$', 'schedule_an_experiment'),
        (r'experiments/doschedule/$', 'schedule_an_experiment'),
        (r'experiments/cancel/(?P<id>\d+)/$', 'cancel_an_experiment'),
        (r'experiments/responsible/$', 'view_responsible'),
        (r'experiments/uncompleted/$', 'view_unmarked_complete'),
        (r'experiments/markcomplete/(?P<id>\d+)/$', 'mark_completed'),
        (r'staff/schedule/$', 'staff_schedule_subject'),
        (r'staff/subjectview/(?P<uname>\w+)/$', 'view_subject'),
        #(r'staff/namecomplete/', 'user_autocomplete'),
        (r'staff/exptcomplete/', 'experiment_autocomplete'),
        (r'staff/roomcomplete/', 'experiment_room_autocomplete'),
        (r'staff/newexperiment/', 'create_new_experiment')
)
