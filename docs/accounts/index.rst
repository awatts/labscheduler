.. _accounts-index:

:mod:`accounts` -- User accounts
================================

.. module:: accounts
   :synopsis: Models related to user accounts
.. moduleauthor:: Andrew Watts <awatts@bcs.rochester.edu>

.. toctree::
   :maxdepth: 1

   models
   views

LabScheduler has user accounts to keep track of demographic and personal information about users.

Installation
============
    1. Put ``'labscheduler.accounts'`` in your `INSTALLED_APPS` setting.
    2. Run the command ``manage.py syncdb``.
