#
# fields.py
#
# Author: Andrew Watts
#
# Copyright (c) 2007-2013, University of Rochester
# This software is licensed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from __future__ import absolute_import
from __future__ import unicode_literals

from django import forms
from .widgets import RaceMultiWidget

class RaceMultiValueField(forms.fields.MultiValueField):
    """
    A subclass of django.forms.fields.MultiValueField to handle input of
    the race information for the user survey.

    See the docs for django.forms.Field for more information.
    """
    def __init__(self, *args, **kwargs):
        fields = (
                forms.CharField(label="American Indian / Alasks Native", max_length=1),
                forms.CharField(label="Black or African-American", max_length=1),
                forms.CharField(label="Native Hawaiian or Other Pacific Islander", max_length=1),
                forms.CharField(label="Asian", max_length=1),
                forms.CharField(label="White", max_length=1),
                forms.CharField(label="Unknown", max_length=1),
                )
        widget = (
                forms.widgets.CheckboxSelectMultiple(choices=(
                    ('1', "American Indian / Alaska Native"),
                    ('2', "Black or African-American"),
                    ('3', "Native Hawaiian or Other Pacific Islander"),
                    ('4', "Asian"),
                    ('5', "White"),
                    ('6', "Unknown"),
                        )
                    )
                )
        super(RaceMultiValueField, self).__init__(fields=fields, widget=widget, *args, **kwargs)

    def compress(self, data_list):
        """
        Superclass expects values to be returned as one object. This merely
        returns the list of values it was passed.

       @returns: A list of the form [bool, bool, bool, bool, bool, bool]
        """
        if data_list:
            #print data_list
            return [x in data_list for x in ['1', '2', '3', '4', '5', '6']]
        return [False, False, False, False, False, False]
