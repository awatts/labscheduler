#
# basic_calendar/views.py
#
# Author: Andrew Watts
#
# Copyright (c) 2008, University of Rochester
# This software is distributed under a BSD style license.
# See the LICENSE file in the top level directory for more information.
#

from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required, user_passes_test
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse
from labscheduler.basic_calendar.models import BasicCalendar, Event
from datetime import date, time, timedelta

@login_required
def index(request):
    """
    Default index page showing the user any events they are involved in for the
    next week
    """
    cal, created = BasicCalendar.objects.get_or_create(owner=request.user, defaults={'earliest': time(9,0), 'latest': time(17,0)})

    start_date = date.today()
    end_date = date.today()+timedelta(7)
        
    events = Event.objects.exclude(event_date__gt=date.today()+timedelta(7)).filter(event_date__gte=date.today(), calendar = cal)

    return render_to_response('calendar/weekview.html', {'user': request.user, 'events': events, 'start_date': start_date, 'end_date': end_date})

@login_required
def list_freetimes(request):
    return HttpResponseBadRequest()

@user_passes_test(lambda u: u.is_staff)
def events_for_day(request):
    """
    Gives staff a list of all events scheduled for a given day
    """

    if request.method != 'GET':
        return HttpResponseBadRequest()

    if request.GET.has_key("date"):
        reqdate = request.GET["date"]
    else:
        reqdate = date.today().isoformat()

    events = Event.objects.filter(event_date = reqdate)

    return render_to_response('calendar/eventsforday.html', {'user': request.user})
