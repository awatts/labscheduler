.. _calendaring-index:

====================================================
:mod:`calendaring` -- Connecting to calendar servers
====================================================

.. module:: calendaring
   :synopsis: Models for connecting to calendar servers
.. moduleauthor:: Andrew Watts <awatts@bcs.rochester.edu>

.. toctree::
   :maxdepth: 1

   basic/index
   darwin/index
   google/index

Purpose
=======

The calendaring module and submodules are supposed to be designed to give a flexible, pluggable backend, so you can use whatever calendaring system you desire or is available to you.

In practice the three partially implemented systems are mostly disparate, and it will take a fair amount of work to unify them and make the system truly pluggable. Currently the only close to working module is :mod:`~labscheduler.calendaring.google`.

Submodules
==========

Basic
-----

The Basic calendar backend is a simple, dumb, not particularly useful calendar that was written as a dummy backend during development.

Darwin
------

Starting with MacOS X 10.5 Server, Apple has provided a calendar server that can be deployed on your network called `Darwin Calendar Server <http://trac.calendarserver.org/>`_. It should also work on Linux (e.g. there's a `Debian package <http://packages.qa.debian.org/c/calendarserver.html>`_), but with less integration than on MacOS X Server, where it authenticates against OpenDirectory. On other platforms you can use a special XML format for storing principal information or Apache's mod_auth.

.. note::

   This backend has not been completed.

Google
------

Google provides an API for connecting to `Google Calendar <http://code.google.com/apis/calendar/>`_, including client libraries for a variety of languages that abstract away from the underlying HTTP and Atom XML format.
