.. _accounts-models:

=================================
LabScheduler user accounts models
=================================

.. module:: labscheduler.accounts.models
   :synopsis: Models related to accounts

Overview
========

The accounts system contains the following models:
    * :class:`Survey`:
      Information about demographics, hearing, vision, and willingness to participate in future experiments
    * :class:`Phone`:
      Contact phone numbers
    * :class:`Email`:
      Contact email addresses
    * :class:`Language`:
      Languages spoken, included competence level and when they started to speak them
    * :class:`DoNotSchedule`:
      An object to track per user, per lab, what subjects are not to scheduled in future experiments, and why

Survey
======

.. class:: Survey

API reference
-------------

Fields
~~~~~~

.. class:: Survey

    :class:`~labscheduler.accounts.models.Survey` objects have the following fields:

    .. attribute:: Survey.user

        Required. A :class:`~django.contrib.auth.models.User` object.

    .. attribute:: Survey.datefilled

        Required. A :class:`datetime.date` object. Default is :func:`datetime.date.today()` When the survey was filled out. This is for keeping track of when new subjects enroll for the purpose of reports to funding agencies and IRBs.

    .. attribute:: Survey.dob

        Required. A :class:`datetime.date` object. Date of Birth.

    .. attribute:: Survey.gender

        Required. A character representing gender

        .. code-block:: python

            GENDER_CHOICES = (
                 (u'M', u'Male'),
                 (u'F', u'Female'),
                 (u'N', u'Not specified'),
                 )

    .. attribute:: Survey.hisp

        Required. Hispanic or Latino? Default is :keyword:`False`

    .. attribute:: Survey.amerind

        Required. American Indian? Default is :keyword:`False`

    .. attribute:: Survey.afram

        Required. African American? Default is :keyword:`False`

    .. attribute:: Survey.pacif

        Required. Native Hawaiian or Pascific Islander? Default is :keyword:`False`

    .. attribute:: Survey.asian

        Required. Asian? Default is :keyword:`False`

    .. attribute:: Survey.white

        Required. White? Default is :keyword:`False`

    .. attribute:: Survey.unk

        Required. Unknown race? Default is :keyword:`False`

    .. attribute:: Survey.race_other

        Optional. Other race. 30 characters or fewer.

    .. attribute:: Survey.student

        Required. Current student? Default is :keyword:`False`

    .. attribute:: Survey.gradyear

        Optional. Expected year of graduation. Positive integer.

    .. attribute:: Survey.gradterm

        Optional. Two characters. Expected term of graduation.

        .. code-block:: python

            TERM_CHOICES = (
                (u'SP', u'Spring'),
                (u'AU', u'Autumn'),
                (u'SU', u'Summer'),
                )

    .. attribute:: Survey.hearing_normal
        
        Required. Boolean. Normal hearing? 

    .. attribute:: Survey.hearing_problems

        Optional. Text field. Describe any hearing problems.

    .. attribute:: Survey.vision_normal

        Required. Boolean. Normal vision?

    .. attribute:: Survey.vision_other

        Required. Vision level.

        .. code-block:: python

            VISION_CHOICES = (
            (0, u'Normal uncorrected'),
            (1, u'Corrected-to-normal with glasses'),
            (2, u'Corrected-to-normal with soft contacts'),
            (3, u'Corrected-to-normal with hard contacts'),
            (4, u'Other'),
            )

    .. attribute:: Survey.more_expts

        Required. Boolean. Willing to particiapte in more experiments?

Methods
~~~~~~~

.. class:: Survey

    .. method:: Survey.__unicode__()

        Returns the unicode form of :attr:`user`

    .. method:: Survey.real_name()

        Returns the first name and last name of :attr:`user` as "Firstname Lastname"

    .. method:: Survey.over_18()

        Returns a Boolean value of whether :attr:`dob` is over 18 years old.

    .. method:: Survey.primary_email()

        Returns the first contact email address for the person

    .. method:: Survey.vision()

        Returns the string form of :attr:`vision_other`

Phone
=====

.. class:: Phone

Phone is a separate class so an arbitrary number of phone numbers can be associated with a single :class:`Survey`.

API reference
-------------

Fields
~~~~~~
.. class:: Phone

    .. attribute:: Phone.survey

        Required. The :class:`Survey` object associated with this phone number.

    .. attribute:: Phone.phone

        Required. A phone number in the North American format

        .. seealso::

            :class:`~django.contrib.localflavor.us.forms.USPhoneNumberField`

    .. attribute:: Phone.type

        Required. What kind of contact phone number.

        .. code-block:: python

           PHONE_CHOICES = (
                (u'home', u'Home'),
                (u'work', u'Work'),
                (u'cell', u'Cell'),
            )

    The :attr:`survey` and :attr:`phone` are required to be unique together.

Methods
~~~~~~~

.. class:: Phone

    .. method:: __unicode__()

        Returns string of the phone number

Email
=====

.. class:: Email

Email is a separate class so an arbitrary number of email addresses can be associated with a single :class:`Survey`.

API reference
-------------

Fields
~~~~~~

.. class:: Email

    .. attribute:: Email.survey

        Required. The :class:`Survey` object associated with this email address.

    .. attribute:: Email.email

        Required. An email address. Checked with a regex.

        .. seealso::

            :class:`~django.db.models.fields.EmailField`

Methods
~~~~~~~

.. class:: Email

    .. method:: __unicode__()

        Returns string of the email address

Language
========

.. class:: Language

Language is a separate class so an arbitrary number of languages can be associated with a single :class:`Survey`.

API reference
-------------

Fields
~~~~~~

.. class:: Language

    .. attribute:: Language.survey

        Required. The :class:`Survey` object associated with this email address.

    .. attribute:: Language.lang

        Required. String of language name. Limited to the following choices:

        .. code-block:: python

            LANGS = (
                (u'English',u'en - English'),
                (u'ASL',u'American Sign Language'),
                (u'Spanish',u'es - Español'),
                (u'French',u'fr - Français'),
                (u'Italian',u'it - Italiano'),
                (u'Portuguese',u'pt - Português'),
                (u'Romanian',u'ro - Română'),
                (u'German',u'de - Deutch'),
                (u'Dutch',u'nl - Nederlands'),
                (u'Danish',u'da - Dansk'),
                (u'Norwegian',u'no - Norsk'),
                (u'Swedish',u'sv - Svenska'),
                (u'Icelandic',u'is - Íslenska'),
                (u'Finnish',u'fi - Suomi'),
                (u'Yiddish',u'yi - ייִדיש'),
                (u'Hebrew',u'he - עברית'),
                (u'Greek',u'el - Ελληνικα'),
                (u'Russian',u'ru - Русский'),
                (u'Polish',u'pl - Polski'),
                (u'Ukrainian',u'uk - Українська'),
                (u'Turkish',u'tr - Türkçe'),
                (u'Hungarian',u'hu - Magyar'),
                (u'Chinese (Mandarin)',u'cmn - 中文 (官話)'),
                (u'Chinese (Cantonese)', u'yue - 中文 (粵語)'),
                (u'Japanese',u'ja - 日本語'),
                (u'Korean',u'ko - 한국어'),
                (u'Vietnamese',u'vi - Tiếng Việt'),
                (u'Thai',u'th - ภาษาไทย'),
                (u'Tagalog',u'tl - Tagalog'),
                (u'Hindi',u'hi - हिन्दी'),
                (u'Bengali',u'bn - Bengali'),
                (u'Gujarati',u'gu - ગુજરાતી'),
                (u'Urdu',u'ur - اُردو'),
                (u'Kannada',u'kn - Kannada'),
                (u'Punjabi',u'pa - Punjabi'),
                (u'Marathi',u'mr - 	मराठी'),
                (u'Malayalam',u'ml - Malayalam'),
                (u'Telugu',u'te - Telugu'),
                (u'Tamil',u'ta - தமிழ்'),
                (u'Arabic',u'ar - العربية'),
                (u'Persian',u'fa - فارسی'),
                (u'Pashto',u'pa - پښتو'),
            )

    .. attribute:: Language.started

        Required. A :class:`datetime.date` object. When you started speaking the language. Should (by convention) be set to same as :attr:`Survey.dob` for a first language.

    .. attribute:: Language.stopped

        Optional. A :class:`datetime.date` object. When you stopped speaking the anguage, if you no longer speak it.

    .. attribute:: Language.proficient

        Required. Integer. Self rating of level of proficiency in the language.

        .. code-block:: python

            PROFICIENTY_CHOICES = (
                (4, u'Native'),
                (3, u'Near-native fluency'),
                (2, u'Competent'),
                (1, u'Novice'),
            )

    .. attribute:: Language.english_country

        Optional. A string with the name of the country where you learned English *iff* you learned it outside the United States.

    The :attr:`survey` and :attr:`lang` attributes are required to be unique together.

Methods
~~~~~~~

.. class:: Language

    .. method:: __unicode__()

        Returns string of the language name

    .. method:: proficiency()

        Returns the string form of :attr:`Language.proficient`

DoNotSchedule
=============

.. class:: DoNotSchedule

The DoNotSchedule class allows a lab to designate a person as not to be scheduled in future experiments, with an explanation as to why, e.g. incapable of using a computer, harassment of lab staff, etc.

API reference
-------------

Fields
~~~~~~

.. class:: DoNotSchedule

    .. attribute:: DoNotSchedule.user

        Required. A :class:`~django.contrib.auth.models.User` object. The person who is not to be scheduled.

    .. attribute:: DoNotSchedule.lab

        Required. A :class:`~labscheduler.resources.models.other.Laboratory` object. Which lab this applies to.

    .. attribute:: DoNotSchedule.note

        Optional. Free form text of why this person is not to be scheduled anymore.

Methods
~~~~~~~

.. class:: DoNotSchedule
