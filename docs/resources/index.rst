.. _resources-index:

:mod:`resources` -- Schedulable resources
=========================================

.. module:: resources
   :synopsis: Models related to schedulable resources
.. moduleauthor:: Andrew Watts <awatts@bcs.rochester.edu>

.. toctree::
   :maxdepth: 1

   models
   views
